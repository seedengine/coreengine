#include "File.h"
#include "Exceptions.h"
#include <iostream>

File::File(std::string name) : file(), name(name), size(0)
{
}

void File::load()
{
	load( std::fstream::in | std::fstream::out | std::fstream::binary);
}

void File::reload(std::string _name)
{
	reload( _name, std::fstream::in | std::fstream::out | std::fstream::binary);
}

void File::unload()
{
	file.close();
	file_it = std::istreambuf_iterator<char>();
	size = 0;
}


size_t File::read(void* buf, size_t size)
{
	file.read((char*)buf, size); //possibly needs syncronisation guarantee
	return (size_t)file.gcount();
}

void File::readAt(void* buf, size_t size, unsigned pos)
{
	if (pos >= size)
		throw FrameworkException("Index out of Bounds");

	file.seekg(pos);
	read(buf, size);
}

void File::write(char* buf, size_t size)
{
	file.write(buf, size);
}

void File::writeAt(char* buf, size_t size, unsigned pos)
{
	if (pos > size)
		throw FrameworkException("Index out of Bounds");

	file.seekg(pos);
	write(buf, size);
}

std::streampos File::getSize()
{
	return size;
}

std::string File::getPath()
{
	return name;
}

void File::rename(std::string _name)
{
	unload();
	std::rename(name.c_str(), _name.c_str());
	reload(_name);
}

void File::move(std::string _name)
{
	copy(_name);
	remove();
}

void File::copy(std::string _name)
{
	std::ofstream out(_name);
	out << file.rdbuf();
}

void File::remove()
{
	unload();
	std::remove(name.c_str());
}

std::istreambuf_iterator<char> File::begin()
{
	return file_it;
}

File::~File()
{
}

void File::load(std::ios::openmode access)
{
	if (file.is_open())
		throw FrameworkException("File unexpectantly already opened.");

	file.open(name, access | std::fstream::ate);
	size = file.tellg();
	file.seekg(0);
	file_it = std::istreambuf_iterator<char>(file);
}

void File::reload(std::string _name, int access)
{
	if (file.is_open())
		unload();
	name = _name;
	load();
}

std::fstream& File::_getHandle()
{
	return file;
}
