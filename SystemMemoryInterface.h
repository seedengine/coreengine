//Why does IntelliSense see Errors here
#ifndef SYS_MEM_IF
	#define SYS_MEM_IF
	#include <assert.h>
	#include "MemoryAssignment.h"
	#include "IMemoryInterface.h"

	class SystemMemoryInterface : public IMemoryInterface<> //Class for operating on System Memory
	{
	public:
		SystemMemoryInterface(size_t blockSize) : IMemoryInterface<>(blockSize), DATA_SIZE(blockSize)
		{

		}

		MemoryRange allocate(size_t)
		{
			allocations.push_back(malloc(DATA_SIZE));
			return MemoryRange((uint8_t)allocations.size() - 1, 0, DATA_SIZE);
		}
		void freeRange(MemoryRange memory)
		{
			free(allocations[memory.memIndex]);
		}

		inline size_t getSize() { return DATA_SIZE; }

		void set( MemoryRange area, void* data)
		{
			memcpy(
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[area.memIndex]) + area.offset),
				data,
				area.size);
		}
		void set(MemoryRange area, void* data, size_t offset, size_t size)
		{
			memcpy(
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[area.memIndex]) + area.offset + offset),
				data,
				size);
		}

		MemoryRange copy(MemoryRange src, MemoryRange dst)
		{
			assert(src.size < dst.size);
			memcpy(
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[dst.memIndex]) + dst.offset), 
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[src.memIndex]) + src.offset),
				src.size);
			return dst;
		}
		void swap(MemoryRange src, MemoryRange dst)
		{
			assert(src.size < dst.size);
			memcpy(
				allocations[src.memIndex],
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[dst.memIndex]) + dst.offset),
				dst.size);
			copy(src, dst);
			memcpy(
				reinterpret_cast<void*>(reinterpret_cast<uint64_t>(allocations[src.memIndex]) + src.offset),
				allocations[src.memIndex],
				src.size);
		}

		~SystemMemoryInterface()
		{
			free(buffer);
		}
	private:
		const size_t DATA_SIZE;
		void* buffer;
	};
#endif
