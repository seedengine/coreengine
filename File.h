#include <fstream>
#include <string>

#pragma once
class File //File Wrapper
{
public:
	File(std::string name);

	virtual void load();
	void reload( std::string _name);
	virtual void unload();

	size_t read(void*, size_t);
	void readAt(void*, size_t, unsigned);

	void write(char*, size_t);
	void writeAt(char*, size_t, unsigned);

	std::streampos getSize();
	std::string getPath();

	void rename( std::string);
	void move( std::string);
	void copy( std::string);
	void remove();

	std::istreambuf_iterator<char> begin();

	~File();
protected:
	void load(std::ios::openmode access);
	void reload(std::string _name, int access);
	std::fstream& _getHandle();
private:
	std::fstream file;
	std::istreambuf_iterator<char> file_it;
	std::string name;
	std::streampos size;
};