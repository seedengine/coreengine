
//actually ad scheduling this time
#ifndef THREAD
	#define THREAD
	#include <thread>
	#include <vector>
	#include <functional>
	#include <algorithm>
	#include <mutex>
	#include <queue>
	#include <chrono>
	#include "Barrier.h"
	#include <iostream>
	
class ComponentInterface;

	struct Task //Stores Task Metadata for execution
	{
		Task() {} //Default constructor to allocate Arrays
		Task(std::function<void()> callable) :
			callable(callable),
			time(1)
		{

		}

		//Getter and Setter for the Invokation
		void setTime(const std::chrono::system_clock::duration& dur)
		{
			time = dur;
		}
		std::chrono::system_clock::duration getTime()
		{
			return time;
		}

		//Invokation
		void operator()()
		{
			if (callable)
				callable();
		}

		//time comparison
		bool operator<(const Task& other) const
		{
			return time < other.time;
		}
		bool operator>(const Task& other) const
		{
			return time > other.time;
		}
	private:
		std::function<void()> callable; //the Function to be Invokated
		std::chrono::system_clock::duration time; //tracker to store the time the last invokation took. This is for later usage to budget frame time in order to avoid lag from bug but infrequent tasks.
	};
	struct ScheduledTask //Stores additional data for task that are automatically rescheduled
	{
		ScheduledTask(Task* t, bool main) : task(t), main(main)
		{}

		Task* task;
		bool main; //Required to be executed in main thread?, Move to Task maybe
		std::chrono::system_clock::duration avgTime; //for each tick the relevance of all previous measurements is halfed
	};

	//Manages an arbitrary amount of worker threads and schedules work for them
	class ThreadPool
	{
	public:
		ThreadPool(uint8_t cores, ComponentInterface*);
		//The ThreadPool can't be copied
		ThreadPool(const ThreadPool& other) = delete;
		void operator=(const ThreadPool& other) = delete;

		~ThreadPool();

		//needs Terminator
		
		inline void run()
		{
			tickGuard.setBarrierSize(cores + 1);
			work(true); //joins the main thread with the the work pool
		}
		inline void stop() //disables subsequent cycles. terminates threadpool
		{
			execution = false;
		}
		void flush();

		//should return Handle
		void listSingleTask(const Task& t, bool main = false); //adds a task to be schedule in the next cycle
		void listSingleTask(Task&& t, bool main = false); //adds a task to be schedule in the next cycle
		void listImmediateTask(const Task& t); //adds a task to be schedule in the next cycle
		void listImmediateTask(Task&& t); //adds a task to be schedule in the next cycle
		void listLowFreqTask(const ScheduledTask& t); //add new scheduled Tast to list for a low frequency update (20 TPS)
		void listHighFreqTask(const ScheduledTask& t); //add new scheduled Tast to list for a high frequency update (60 TPS)

	private:
		void work(bool main);

		void scheduleHigh(); //schedule high frequency tasks
		void scheduleLow(); //schedule low frequency tasks

		//per thread queues that are balanced to avoid synchronization?
		std::priority_queue<Task> taskQueue;
		std::priority_queue<Task> otherTaskQueue;

		std::recursive_mutex activeQueueMut;
		//std::recursive_mutex idleQueueMut;
		std::priority_queue<Task>* activeTaskQueue;
		std::priority_queue<Task>* idleTaskQueue;


		std::recursive_mutex mainQueueMut;
		std::priority_queue<Task> mainTaskQueue;

		std::mutex scheduling;
		std::mutex scheduleTaskLists;
		std::chrono::system_clock::time_point lastLowTick, lastHighTick;
		std::vector<ScheduledTask> highFreqTasks; //respectively run at 60/20 tps, force executed in Main Thread
		std::vector<ScheduledTask> lowFreqTasks; //respectively run at 60/20 tps

		const uint32_t cores;
		std::vector<std::thread> threads;
		std::atomic_bool execution;
		Barrier tickGuard;

		float idleTime;

		ComponentInterface* coreIf;
	};
#endif
