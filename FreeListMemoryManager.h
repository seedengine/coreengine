
#ifndef FREE_LIST_MEM_MGR
	#define FREE_LIST_MEM_MGR
	#include <type_traits>
	#include <cmath>
	#include <assert.h>

	#include "IMemoryInterface.h"
	#include "IMemoryManager.h"
	#include "MemoryAssignment.h"
	#include "Exceptions.h"

	#include "Util.h"

	#pragma once
	struct BlockAssignment //lock individual pool to make them threadsafe?
	{
		BlockAssignment(size_t poolIndex, uint32_t minBlockSize) :
			poolIndex(poolIndex),
			blocks{ 0,0,0,0,0,0,0,0 },
			lru{ 0,0,0,0,0,0,0 },
			MIN_BLOCK_SIZE(minBlockSize)
		{}


		//returns -1 if custom allocation is required
		static inline constexpr uint8_t getSizeIndex(const uint32_t MIN_BLOCK_SIZE, uint64_t size)
		{
			if (size <= MIN_BLOCK_SIZE)
			{
				return 0;
			}
			if (size > 256 * MIN_BLOCK_SIZE)
			{
				return -1;
			}

			size /= MIN_BLOCK_SIZE;

			return (uint8_t)fastLog2(size);
		}

		inline bool isFree(uint8_t sizeIndex)
		{
			return blocks[sizeIndex + 1] != 0xFFFFFFFFFFFFFFFFull;
		}

		inline uint8_t LRUStep(uint8_t sizeIndex, uint8_t offset = 0, uint8_t depth = 0) //inline recursion to avoid jumps
		{
			uint8_t opOffset = (uint8_t)pow(2, depth) + offset;

			if (opOffset > 32)
			{
				if ((lru[sizeIndex] >> (opOffset - 1)) & 1)
				{
					lru[sizeIndex] ^= 1ull << (opOffset - 1);
					return (opOffset * 2) - 64;
				}
				else
				{
					lru[sizeIndex] ^= 1ull << (opOffset - 1);
					return (opOffset * 2) - 63;
				}
			}
			if ((lru[sizeIndex] >> opOffset) & 1)
			{
				lru[sizeIndex] ^= 1ull << opOffset;
				return LRUStep(sizeIndex, offset * 2 + 1, depth + 1);
			}
			else
			{
				lru[sizeIndex] ^= 1ull << opOffset;
				return LRUStep(sizeIndex, offset * 2, depth + 1);
			}
		}

		uint8_t getFreeOffset(uint8_t sizeIndex)
		{
			if (sizeIndex == 7)
				return 0;
			uint8_t dataPos = LRUStep(sizeIndex);
			while (((blocks[sizeIndex] >> dataPos) & 1)) //find first free slot
				dataPos = LRUStep(sizeIndex);
			blocks[sizeIndex] |= 1ull << dataPos; //Lock Slot
			return dataPos;
		}

		size_t getPoolIndex()
		{
			return poolIndex;
		}

		void free(uint8_t sizeIndex, uint64_t offset)
		{
			blocks[sizeIndex] &= !(1 << (offset / (sizeIndex > 0 ? (MIN_BLOCK_SIZE << (sizeIndex - 1)) : (MIN_BLOCK_SIZE >> -(sizeIndex - 1)))));
		}
	private:
		size_t poolIndex;
		//block [0] is a block for swapping
		//blocks have 64 allocations
		//[1] - [7] are allocations from MIN_SIZE * 2^(index-1)
		uint64_t blocks[8];
		uint64_t lru[7];
		const uint32_t MIN_BLOCK_SIZE;
	};

	//Free List Based Manager
	template<class Interface>
	class FreeListMemoryManager : public IMemoryManager<Interface, BlockAssignment>
	{
	public:
		FreeListMemoryManager(uint32_t minBlockSize) : IMemoryManager<Interface, BlockAssignment>(minBlockSize, minBlockSize * 32/*amount of alocations per set*/ * 252/*sum of 2^n where n are the different allocations*/)
		{
			static_assert(std::is_base_of<IMemoryInterface<typename Interface::AllocationType>, Interface>::value, "Provided Template is not a Memory Interface"); //migrate other checks in the same manner
		}
		~FreeListMemoryManager()
		{

		}


		virtual MemoryRange allocate(size_t size)
		{
			size = getNextPower(size);
			if (size > this->MIN_BLOCK_SIZE * 1 << 7)
				throw FrameworkException("Cannot allocate this much memory");
			uint8_t sizeIndex = BlockAssignment::getSizeIndex(this->MIN_BLOCK_SIZE, size);
			BlockAssignment* block = getNextFreeBlock(sizeIndex);
			size_t pos = block->getFreeOffset(sizeIndex) * (this->MIN_BLOCK_SIZE << sizeIndex );

			return MemoryRange(block->getPoolIndex(), pos, size);
		}
		void clearMemoryRange(MemoryRange range)
		{
			if (range.memIndex == -1)
				return;
			this->allocationPools[range.memIndex].free(BlockAssignment::getSizeIndex(this->MIN_BLOCK_SIZE, range.size), range.offset);
		}



	private:
		constexpr inline uint32_t getSize(uint32_t minBlockSize)
		{
			uint32_t size{ 0 };
			for (unsigned i = 0; i < 7; i++)
				size += (minBlockSize << i) * 64;
			return size;
		}

		constexpr inline bool checkPwr(uint64_t num) {
			if (num == 0)
				return false;
			return !(num & (num - 1));
		}
		constexpr size_t getNextPower(size_t x) {
			if (x == 0)
				return 1;
			else if (x < this->MIN_BLOCK_SIZE)
				return this->MIN_BLOCK_SIZE;
			else if (checkPwr(x))
				return x;

			x--;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 16;
			x++;

			return x;
		}
		
		inline BlockAssignment* allocate()
		{
			this->allocationPools.push_back(BlockAssignment(this->memoryInterface.allocate(0 /*argument ignored*/).memIndex, this->MIN_BLOCK_SIZE));
			return &this->allocationPools[this->allocationPools.size() - 1];
		}

		BlockAssignment* getNextFreeBlock(uint8_t sizeIndex)
		{
			BlockAssignment* freeBlock = nullptr;
			for (BlockAssignment& block : this->allocationPools)
			{
				if (block.isFree(sizeIndex))
				{		
					freeBlock = &block;
					break;
				}
			}

			if (!freeBlock)
				freeBlock = allocate();
			return freeBlock;
		}

	};
#endif