#include "Event.hpp"
#include "IEngine.h"
#include "OrderedList.hpp"

#include <iostream>
#include <mutex>

//Define Static Elements
std::atomic<uint64_t> EventHandler::lastID = 0ull;


EventHandler::~EventHandler()
{
	core->getEventBus()->removeHandler(id);
}

EventBus::EventDependencyBase::EventDependencyBase(ComponentInterface* core, std::set<uint64_t> data) : EventHandler(core), core(core), events(data)
{
}

void EventBus::EventDependencyBase::operator()(ComponentInterface* core, const std::shared_ptr<EventBase> evt, bool isSync)
{
	events.erase(evt->getID());
	if (events.empty())
	{
		invoke(core);
	}
}

EventBus::EventDependencyBase& EventBus::EventDependencyBase::operator=(const EventBus::EventDependencyBase& other)
{
	for (uint64_t entry : other.events)
		core->getEventBus()->removeHandler(this->getHandlerID());
	events.clear();

	for (uint64_t entry : other.events)
		core->getEventBus()->registerDependencyHandler(this, entry);
	events = other.events;
	return *this;
}

void EventBus::EventDependencyBase::construct(ComponentInterface* core)
{
	std::set<uint64_t> data = events;
	for (uint64_t entry : data)
		core->getEventBus()->registerDependencyHandler(this, entry);
}

void EventBus::registerDependencyHandler(EventBus::EventDependencyBase* handler, uint64_t typeID)
{
	if (!running)
		throw FrameworkException("Attempting to use terminated EventBus.");
	std::unique_lock<std::recursive_mutex> guard(eventMutex);
	if (invokedDependencies.find(typeID) == invokedDependencies.end())
	{
		dependencyList.insert(std::move(EventHandlerRef{ typeID, handler->getHandlerID(), handler}));
	}
	else
	{
		std::shared_ptr<DummyEvent> ptr = constructEvent<DummyEvent, uint64_t>(core, typeID);
		handler->operator()(core, ptr);
	}
}


EventBus::EventBus(ComponentInterface* core) : core(core), running(true)
{
}

const MemoryRange EventBus::createFence(ComponentInterface * core)
{
	MemoryRange range = core->getMemoryHandler()->allocate(sizeof(std::atomic<bool>));
	std::atomic<bool>* fenceState = (std::atomic<bool>*)core->getMemoryHandler()->getData(range);
	std::atomic_init(fenceState, false);
	return range;
}

void EventBus::destroy()
{
	std::unique_lock<std::recursive_mutex> lock0(eventMutex);
	running = false;
	if (!dependencyContainer.empty()) //arcane code amgic. no clue why its necessary
	{
		for (auto dep : dependencyContainer)
			delete dep.second;
		dependencyContainer.clear();
	}
	eventHandler.clear();
	dependencyList.clear();
	invokedDependencies.clear();
}

EventBus::~EventBus()
{
	if (running) //WHAT
		throw FrameworkException("Attempting to destroy EventBus without shutting it down first.");
}

void EventBus::postEventInvokation(ComponentInterface * core, const std::shared_ptr<EventBase> evtData)
{
	std::unique_lock<std::recursive_mutex> guard(eventMutex);

	std::set<uint64_t> removeSet;

	for (auto id : removeBuffer)
		for (auto it : eventHandler)
			for (size_t i = 0; i < it.second.size(); i++)
				if (it.second[i].id == id)
					removeSet.insert(it.first);

	for (auto evt : removeSet)
	{
		std::vector<EventHandlerRef>& vec = eventHandler[evt];

		for (auto id = removeBuffer.rbegin(); id != removeBuffer.rend(); id++)
		{
			size_t i = 0;
			for (; i < vec.size(); i++)
				if (vec[i].id == *id)
					break;
			if (i < vec.size() && vec[i].id == *id)
				vec.erase(vec.begin() + i);
		}
		eventHandler[evt] = vec;
	}
	removeSet.clear();

	for (auto id : removeBuffer)
	{
		for (size_t i = 0; i < dependencyList.size(); i++)
			if (dependencyList[i].id == id)
				removeSet.insert(i);

		for (auto it = removeSet.rbegin(); it != removeSet.rend(); it++)
			dependencyList.erase((size_t)*it);
		removeSet.clear();
	}

	removeBuffer.clear();


	//EventBase* evtData = (EventBase*)core->getMemoryHandler()->getData(evt);
	const EventHandlerRef nullRef = EventHandlerRef{ evtData->getID(), (uint64_t)-1, nullptr }; //Reference with invalid handler
	size_t pos = dependencyList.getPos(nullRef, 0, dependencyList.size() - 1);//match all entries using the compareRefType function. It doesn't compare event handlers.

	while (pos < dependencyList.size() && pos >= 0 && dependencyList[pos].type == evtData->getID())
	{
		if ( removeBuffer.find(dependencyList[pos].id) == removeBuffer.end())
			(*dependencyList[pos].ref)(core, evtData, true);
		pos++;
	}

	for (auto dependency : dependencyContainer)
	{
		if (!dependency.second->isClear())
			continue;
		delete dependencyContainer[dependency.first];
		removeSet.insert(dependency.first);
	}
	for (auto it = removeSet.rbegin(); it != removeSet.rend(); it++)
		dependencyContainer.erase(*it);

	invokedDependencies.insert(evtData->getID());
}

void EventBus::invokeEvent(ComponentInterface* core, const std::shared_ptr<EventBase> evtData)
{
	std::unique_lock<std::recursive_mutex> lock(eventMutex);

	std::vector<EventHandlerRef>& itVec = eventHandler[evtData->getID()];

	if (!itVec.empty() && running)
	{
		for (auto it : itVec)
		{
			if (running && removeBuffer.find(it.id) == removeBuffer.end())
			{
				(*it.ref)(core, evtData, false);
			}
		}
	}
	//update dependencies afterwards, next tick is guaranteed to come after event computation
	core->getThreadPool()->listSingleTask(Task([this, core, evtData]() {
		postEventInvokation(core, evtData);
	} ));
	
}


void EventBus::registerHandler(EventHandler* handler, uint64_t typeID)
{
	if (!running)
		throw FrameworkException("Attempting to use terminated EventBus.");
	std::unique_lock<std::recursive_mutex> guard(eventMutex);
	eventHandler[typeID].push_back(std::move(EventHandlerRef{ typeID, handler->getHandlerID(), handler}));
}

void EventBus::removeHandler(const EventHandle& id)
{
	removeBuffer.insert(id);
}

inline bool EventHandler::operator==(const EventHandler& other) const
{
	return id == other.id;
}
inline bool EventHandler::operator!=(const EventHandler& other) const
{
	return id != other.id;
}
