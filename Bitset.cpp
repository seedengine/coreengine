#include "Bitset.h"

void Bitset::push(bool data)
{
	if ((currSize - (currSize % blockSize)) / blockSize >= dataArr.size())
		dataArr.push_back(0);
	if (data)
		dataArr[(currSize - (currSize % blockSize)) / blockSize] |= 1 << currSize % blockSize;
	else
		dataArr[(currSize - (currSize % blockSize)) / blockSize] &= !(1 << currSize % blockSize);
}

size_t Bitset::size()
{
	return currSize;
}

const bool Bitset::at(size_t offset)
{
	return (dataArr[(currSize - (currSize % blockSize)) / blockSize] >> (currSize % blockSize)) & 1;
}

const bool Bitset::operator[](size_t i)
{
	return (dataArr[(currSize - (currSize % blockSize)) / blockSize] >> (currSize % blockSize)) & 1;
}
