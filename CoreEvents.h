#include <string>

#pragma once
#ifndef CORE_EVENTS
#define CORE_EVENTS

DEFINE_EVENT(StartupEvent)};
DEFINE_EVENT(ShutdownEvent)};

DEFINE_EVENT(LoadEvent)};

DEFINE_EVENT(ModuleInitPreparationEvent)
public:
	ModuleInitPreparationEvent(std::set<uint64_t>* ids) : ids(ids) {}
	void addDependentEvent(uint64_t id) { ids->insert(id); }
private:
	std::set<uint64_t>* ids;
};
DEFINE_EVENT(ModuleInitEvent)};

DEFINE_EVENT(UpdateEvent)};

DEFINE_EVENT(CommandInputEvent)
public:
	void setCommand(std::string str) { command = str; } //unsave? or protected by the const marker?
	std::string getCommand() const { return command; }
private:
	std::string command;
};
#endif
