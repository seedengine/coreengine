
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
#include <map>
#include <functional>
#include <chrono>

#include "IObjectComponent.h"


//Define abstract memory file that is a collection of identified n-dimensional integers


#define WIN

#ifdef WIN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN
#else

#endif

#pragma once
class DynamicCoreEngine : public ComponentInterface, public EventHandler
{
	#ifdef WIN
		typedef HINSTANCE DynLibHandle;
	#else
		typedef void* DynLibHandle;
	#endif
	struct Library
	{
		DynLibHandle handle;
		std::string name;
		std::string fileName;
		std::chrono::time_point<std::chrono::steady_clock> lastUse; //if -1 then always required
	};
	struct ModuleHandle
	{
		IEngineModule* modulePtr;
		Library* container;
		bool initialized = false;
	};
public:
	DynamicCoreEngine(bool);

	template<typename ReturnType, typename... Args>
	std::enable_if_t<std::is_void<ReturnType>::value, void> invoke(std::string libName, std::string function, Args...);
	template<typename ReturnType, typename... Args>
	std::enable_if_t<std::is_pointer<ReturnType>::value, ReturnType> invoke(std::string libName, std::string function, Args...);
	template<typename ReturnType, typename... Args>
	std::enable_if_t<std::conjunction<std::is_trivially_constructible<ReturnType>, std::negation<std::is_void<ReturnType>>, std::negation<std::is_pointer<ReturnType>>>::value, ReturnType> invoke(std::string libName, std::string function, Args...);


	IObjectComponent* createComponent(std::string libName, std::string component, void* data);
	void destroyComponent(std::string libName, IObjectComponent* obj, std::string component);
	IEngineObject* createObject(std::string libName, std::string component, void* data);
	void destroyObject(std::string libName, IEngineObject* obj, std::string component);
	IEngineModule* createEngineModule(std::string libName);
	void addEngineModule(IEngineModule* mod);
	void removeEngineModule(std::string name);
	//get specific Engine Modules
	
	void run();
	virtual void operator()(ComponentInterface*, const std::shared_ptr<EventBase> args, bool isSync);

	virtual IEngineModule* getModule(std::string id);
	virtual void* getDataOnType(size_t i) { return this; }

	~DynamicCoreEngine();
private:
	Library loadLibrary(const std::string&) const;
	void unloadLibrary(Library&&) const;
	template<typename ReturnType, typename... Args>
	ReturnType invokeFunction(DynLibHandle& handle, std::string name, Args...) const;
	template<typename ReturnType, typename... Args>
	std::function<ReturnType(Args...)> getFunction(const DynLibHandle& handle, std::string name) const;

	bool initialized;
	void initModule(std::string);

	std::map<std::string, Library> loadedLibraries;
	std::map<std::string, ModuleHandle> loadedModules;


	std::set<EventHandle> moduleEvents;
};
