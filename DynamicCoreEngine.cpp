#include "DynamicCoreEngine.h"
#include "DynamicCoreEngine.hpp"
#include "Event.h"
#include "Event.hpp"
#include "CoreEvents.h"
#include <locale>
#include <codecvt>
#include <string>
#include <iostream>


DEFINE_EVENT(ModuleInitSetupEvent)};

DynamicCoreEngine::DynamicCoreEngine(bool debugState) :
	ComponentInterface(debugState), EventHandler(this), initialized(false)
{
	getEventBus()->registerHandler(this, ModuleInitSetupEvent::getClassID());
}


IObjectComponent * DynamicCoreEngine::createComponent(std::string libName, std::string component, void* data)
{
	IObjectComponent* comp = nullptr;
	comp = invoke<IObjectComponent*, ComponentInterface*, std::string, void*>(libName, "createComponent", this, component, data);
	return comp;
}

void DynamicCoreEngine::destroyComponent(std::string libName, IObjectComponent * obj, std::string component)
{
	invoke<void, ComponentInterface*, IObjectComponent*, std::string>(libName, "destroyComponent", this, obj, component);
}

IEngineObject * DynamicCoreEngine::createObject(std::string libName, std::string component, void* data)
{
	IEngineObject* comp = nullptr;
	comp = invoke<IEngineObject*, ComponentInterface*, std::string, void*>(libName, "createObject", this, component, data);
	return comp;
}

void DynamicCoreEngine::destroyObject(std::string libName, IEngineObject * obj, std::string component)
{
	invoke<void, ComponentInterface*, IEngineObject*, std::string>(libName, "destroyObject", this, obj, component);
}

IEngineModule * DynamicCoreEngine::createEngineModule(std::string libName)
{
	return getModule(libName);
}

void DynamicCoreEngine::addEngineModule(IEngineModule * mod)
{
	loadedModules.insert(std::pair<const std::string, ModuleHandle>(mod->getID(), ModuleHandle{ mod, nullptr }));
}

void DynamicCoreEngine::removeEngineModule(std::string name)
{
	loadedModules.erase(name);
}

void DynamicCoreEngine::run()
{

	//add core events
	moduleEvents.insert(StartupEvent::getClassID());
	moduleEvents.insert(LoadEvent::getClassID());
	//ask all existing entities for their dependencies
	ModuleInitPreparationEvent evt1{&moduleEvents};
	ModuleInitSetupEvent evt2;
	getEventBus()->dispatchEvent(this, evt2, { ModuleInitPreparationEvent::getClassID() }); //fired when Instance is defined
	getEventBus()->dispatchEvent(this, evt1); //fired when Instance is defined

	//system starting up
	StartupEvent evt;
	eventBus.dispatchEvent(this, evt);
	LoadEvent evt0;
	eventBus.dispatchEvent(this, evt0);

	for (auto mod : loadedModules)
	{
		initModule(mod.first);
	}

	initialized = true;
	eventBus.registerHandler(this, ShutdownEvent::getClassID()); //registers ShutdownHandler

	pool.run();

	for (auto mod = loadedModules.rbegin(); mod != loadedModules.rend(); ++mod)
	{
		this->getLog()->writeInfo(std::string("Destroying " + mod->first).c_str());
		mod->second.modulePtr->destroy();
	}
	pool.flush();
}
void DynamicCoreEngine::operator()(ComponentInterface*, const std::shared_ptr<EventBase> args, bool isSync)
{
	if (args->is<ShutdownEvent>())
	{
		pool.stop();
	}
	else if (args->is<ModuleInitSetupEvent>())
	{
		ModuleInitSetupEvent evt;
		core->getEventBus()->dispatchEvent(core, evt, moduleEvents);
	}
}

void DynamicCoreEngine::initModule(std::string name)
{
	if (loadedModules[name].initialized)
		return;
	this->getLog()->writeInfo(std::string("Initializing Module " + name).c_str());
	loadedModules[name].modulePtr->construct(this);
	loadedModules[name].initialized = true;
}

IEngineModule* DynamicCoreEngine::getModule(std::string id)
{
	if (loadedModules.find(id) == loadedModules.end())
	{
		IEngineModule* mod = nullptr;
		mod = invoke<IEngineModule*, ComponentInterface*, std::string>(id, "createModule", this, id);
		if (mod != nullptr)
		{
			loadedModules.insert(std::pair<const std::string, ModuleHandle>(id, ModuleHandle{ mod, &loadedLibraries[id] }));
			if (initialized)
				initModule(id);
		}
	}
	return loadedModules[id].modulePtr;
}

DynamicCoreEngine::~DynamicCoreEngine()
{
	for (auto mod : loadedModules)
	{
		if (mod.second.container)
			invoke<void, IEngineModule*, ComponentInterface*, std::string>(mod.second.container->name, "destroyModule", mod.second.modulePtr, this, mod.first);
		else
			getLog()->writeError(("Failed to destroy Module " + mod.first + ", because it was added manually from existant source. please unregister it first and then handle destruction.").c_str());
	}
	loadedModules.clear();
	for (auto lib : loadedLibraries)
	{
		unloadLibrary(std::move(lib.second));
	}
	loadedLibraries.clear();
}

#ifdef WIN
DynamicCoreEngine::Library DynamicCoreEngine::loadLibrary(const std::string& path) const
{
	Library lib;
	lib.name = path;
	lib.fileName =  path + ".dll";
	
#ifdef UNICODE
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	#endif
	HINSTANCE handle;

#ifdef UNICODE
	handle = LoadLibrary(converter.from_bytes(lib.fileName).c_str());
	#else
	handle = LoadLibrary(lib.fileName.c_str());
	#endif
	auto err = GetLastError();

	if (handle == NULL)
	{
#ifdef UNICODE
	handle = LoadLibrary(converter.from_bytes("lib" + lib.fileName).c_str());
	#else
	handle = LoadLibrary(("lib" + lib.fileName).c_str());
	#endif
		if (handle != NULL)
			lib.fileName = "lib" + lib.fileName;
	}

	if (handle == NULL)
	{
		throw FrameworkException("Failed to load dynamic Library " + lib.name + ". Error Code: " + std::to_string(err));
	}

	lib.handle = handle;
	lib.lastUse = std::chrono::steady_clock::now();
	return std::move(lib);
}
void DynamicCoreEngine::unloadLibrary(Library&& lib) const
{
	Library l = lib;
	if (l.handle != nullptr)
	{
		if (!FreeLibrary(l.handle))
		{
			throw FrameworkException("Library already destructed or invalid.");
		}
		l.handle = nullptr;
	}
	else
		throw FrameworkException("Library already destructed or invalid.");
}
#else
DynamicCoreEngine::Library DynamicCoreEngine::loadLibrary(const std::string& path) const
{
	Library lib;
	lib.name = path;
	lib.fileName = path + ".so";

	void* handle;
	handle = dlopen(lib.fileName.c_str(), RTLD_NOW);

	if (handle == NULL)
	{
		handle = dlopen(("lib" + lib.fileName).c_str());
		if (handle != NULL)
			lib.fileName = "lib" + lib.fileName;
	}

	if (handle == NULL)
	{
		throw FrameworkException("Failed to load dynamic Library " + lib.name + ".");
	}

	lib.handle = handle;
	lib.lastUse = std::chrono::steady_clock::now();
	return std::move(lib);
}
void DynamicCoreEngine::unloadLibrary(Library&& lib) const
{
	Library l = lib;
	if (l.handle != nullptr)
	{
		dlclose(l.handle);
		l.handle = nullptr;
	}
	else
		throw FrameworkException("Library already destructed or invalid.");
}
#endif
