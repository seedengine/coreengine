#include <type_traits>

#include "DataStructures.h"
#include "Log.h"
#include "ThreadPool.h"
#include "Event.h"
#include "FreeListMemoryManager.h"
#include "SystemMemoryInterface.h"
#include "IFunctionalComponent.h"

#pragma once

#ifndef I_ENGINE

class IEngineModule;

//Base class containing core functionality
class ComponentInterface
{
public:

	virtual IEngineModule* getModule(std::string id) = 0;

	constexpr inline const bool getDebugState()
	{
		return debugState;
	}
	constexpr inline Log* getLog()
	{
		return &logger;
	}
	constexpr inline ThreadPool* getThreadPool()
	{
		return &pool;
	}
	constexpr inline EventBus* getEventBus()
	{
		return &eventBus;
	}
	constexpr inline FreeListMemoryManager<SystemMemoryInterface>* getMemoryHandler() //can i turn this into an IMemoryHandler?
	{
		return &memoryHandler;
	}

	constexpr inline bool isRunning()
	{
		return running;
	}

	inline void stop() {
		running = false;
		eventBus.destroy();
		pool.stop();
	}

	virtual void* getDataOnType(size_t i) = 0;


	~ComponentInterface();
protected:
	ComponentInterface(bool debugState);

	const bool debugState;
	bool running;

	ThreadPool pool;
	Log logger;
	FreeListMemoryManager<SystemMemoryInterface> memoryHandler;
	EventBus eventBus;
};

class IEngineModule :
	public IFunctionalComponent
{
public:
	//interface implemented by the core wrapper
	constexpr inline IEngineModule() = delete;
	IEngineModule& operator= (const IEngineModule& other) = delete;

	virtual void construct(ComponentInterface*) {}

	virtual uint32_t getState() { return -1; }

	virtual void destroy() {}

	virtual const std::string getID() const { return "GENERIC MODULE"; }

	virtual ~IEngineModule() {}

protected:
	explicit inline IEngineModule(ComponentInterface* core) : core(core) {}
	ComponentInterface* core;
};

#define I_ENGINE
#endif 