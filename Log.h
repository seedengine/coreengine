
#ifndef LOG

#include <fstream>
#include <queue>
#include <string>
#include <mutex>

#include "ThreadPool.h"

#pragma once

//Async Logging System, Multi Level
class Log
{
public:
	Log(bool debug, ThreadPool* component);

	void writeInfo(std::string& msg);
	void writeWarning(std::string& msg);
	void writeError(std::string& msg);
	void writeFatal(std::string& msg);

	void writeInfo(const char* msg);
	void writeWarning(const char* msg);
	void writeError(const char* msg);
	void writeFatal(const char* msg);

	void flush();

	inline bool hasLogQueued() { return !(info.empty() && warning.empty() && error.empty() && fatal.empty()); }

	~Log();
private:
	void output(const std::string&);
	void pullMessages();
	
	bool debug;
	Task loggingTask;

	std::ofstream logFile;

	std::queue<std::string> info;
	std::mutex infoMut;
	std::queue<std::string> warning;
	std::mutex warningMut;
	std::queue<std::string> error;
	std::mutex errorMut;
	std::queue<std::string> fatal;
	std::mutex fatalMut;
};

#define LOG
#endif // !LOG

