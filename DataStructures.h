
#include <cstddef>

//uses Macro of Vulkan API to have conform versioning throughout the system. Credit goes to Khronos Inc
#define MAKE_VERSION (major, minor, patch) (((major) << 22) | ((minor) << 12) | (patch))

#ifndef VERSION
	#define VERSION
	#include <type_traits>
	#include <stdio.h>
	#include <string>
	#include <cstring>
	#pragma once

	struct Version //Stores Version in a Vulkan conform System to avoid restructuring and conversion
	{
		constexpr inline Version() : name(""), version(0) {}
		Version(std::string& name, uint32_t version) : name(name.c_str()), version(version) {}
		constexpr Version(const char* name, uint32_t version) : name(name), version(version) {}

		operator const char *() const
		{
			static char string[256] = {}; //Stores returned string
			#if defined(WIN32) || defined(_WINDLL) || defined(_WINDOWS) //uses Windows specific funtions when available
				strcpy_s(string, name); //appends str2 to str1
				strcpy_s(string, " v. ");
				sprintf_s(string, "%u", ((version) >> 22) & ((1 << 11) - 1)); //appends Integer to char sequence
				strcpy_s(string, ".");
				sprintf_s(string, "%u", ((version) >> 12) & ((1 << 11) - 1));
				strcpy_s(string, ".");
				sprintf_s(string, "%u", (version & ((1 << 13) - 1)));
			#else
				strcpy(string, name);
				strcpy(string, " v. ");
				sprintf(string, "%u", ((version) >> 22) & ((1 << 11) - 1));
				strcpy(string, ".");
				sprintf(string, "%u", ((version) >> 12) & ((1 << 11) - 1));
				strcpy(string, ".");
				sprintf(string, "%u", (version & ((1 << 13) - 1)));
			#endif
			puts(string); //adds terminator
			return string;
		}
		const char * name;
		int version;
	};
#endif