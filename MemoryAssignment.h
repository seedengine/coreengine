#include <cstdint>


#ifndef MEM_RANGE
#define MEM_RANGE
#include <type_traits>
#pragma once
struct MemoryRange //Stores the Memory Pool index as well as the allocated Block data for the Memory Management Systems. Essentially a pointer
{
	constexpr inline MemoryRange(size_t memIndex, size_t offset, size_t size) : memIndex(memIndex), offset(offset), size(size)
	{}
	constexpr inline MemoryRange() : memIndex(-1), offset(-1), size(0)
	{}

	explicit constexpr inline operator bool() const
	{
		return memIndex != -1 && offset != -1 && size > 0;
	}

	bool operator!=(const MemoryRange& other) const
	{
		return !(memIndex == other.memIndex && offset == other.offset && size == other.size);
	}

	size_t memIndex;
	size_t offset;
	size_t size;
};
#endif
