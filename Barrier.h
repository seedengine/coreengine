
#ifndef BARRIER
	#define BARRIER
	#include <atomic>
	#include <chrono>
	#include <mutex>
	#include <condition_variable>

	class Barrier
	{
	public:

		//Barrier locks maxWaits Threads and releases them all once the barrier is "full"
		inline Barrier(uint32_t maxWaits) : maxWaits(maxWaits), currentWaits(0)
		{}

		//It is impossible to create an "empty" Barrier.
		Barrier() = delete;
		//Barrier can not be copied. Therefor the copy constructor and the copy operator are deleted
		Barrier(const Barrier& other) = delete;
		Barrier& operator=(const Barrier& other) = delete;

		void wait()
		{
			std::unique_lock<std::mutex> guard(lock);
			if (currentWaits.fetch_add(1) + 1 >= maxWaits) { //check if thread is last. Add + 1 to atomic since old value is returned
				currentWaits.store(0); //concurrent editing impossible, only non blocked barrier thread
				cv.notify_all(); //notified and unlocks all threads
			} 
			else
			{
				cv.wait(guard); //awaits unlocking the lock by the last thread
			}
		}

		void reset()
		{
			currentWaits.store(0); //reset thread counter
			cv.notify_all(); //unlock waiting threads
		}

		inline void setBarrierSize(uint32_t size)
		{
			maxWaits = size;
			reset();
		}

		constexpr inline const uint32_t getBarrrierSize() const
		{
			return maxWaits;
		}
		inline const uint32_t getCurrentWaiting() const
		{
			return currentWaits;
		}
	private:
		std::condition_variable cv;
		std::mutex lock;

		uint32_t maxWaits;
		std::atomic<uint32_t> currentWaits;
	};
#endif
