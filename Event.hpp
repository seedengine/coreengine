#ifndef EVENT_T
#define EVENT_T
#include <mutex>
#include "Event.h"
#include "ThreadPool.h"
#include "IEngine.h"
#include "CoreEvents.h"
#include <iostream>

template<class T>
inline void EventBus::dispatchEvent(ComponentInterface* core, T& evt, std::set<uint64_t> dependencies)
{
	if (!running)
		throw FrameworkException("Attempting to use terminated EventBus.");
	EventDependency<T>* dep = new EventDependency<T>(core, evt, dependencies);
	dependencyContainer.insert(std::pair<EventHandle, EventDependencyBase*>(dependencyIDCounter++, dep));
	dep->construct(core);
}

template<typename T>
void EventBus::dispatchEvent(ComponentInterface* core, T& evtData)
{
	if (!running)
		throw FrameworkException("Attempting to use terminated EventBus.");
	std::unique_lock<std::recursive_mutex> guard(eventMutex);

	std::shared_ptr<T> ptr = constructEvent<T>(core, evtData);

	Task t = Task([core, ptr, this] {
		if (running)
			invokeEvent(core, ptr);
	});

	core->getThreadPool()->listSingleTask(t);

}
template<typename T>
void EventBus::invokeEventSynchonous(ComponentInterface* core, T& evtData)
{
	std::unique_lock<std::recursive_mutex> guard(eventMutex);

	std::shared_ptr<T> ptr = constructEvent<T>(core, evtData);
	for (auto it : eventHandler[evtData.getID()])
		if (removeBuffer.find(it.id) == removeBuffer.end())
			(*it.ref)(core, ptr, true);

	postEventInvokation(core, ptr);
}

template<class T /*determines event type*/, typename... Args>
static std::enable_if_t<std::is_base_of<EventBase, T>::value, std::shared_ptr<T>> EventBus::constructEvent(ComponentInterface* core, Args... args)
{
	MemoryRange memRng = core->getMemoryHandler()->allocate(sizeof(T));
	T* dataPos = (T*)core->getMemoryHandler()->getData(memRng);
	new(dataPos) T(args...);
	std::shared_ptr<T> ptr = std::shared_ptr<T>(dataPos, [core, memRng](T* var) {
		var->~T();
		core->getMemoryHandler()->clearMemoryRange(memRng); 
	});
	return ptr;
}

template<class T>
EventBus::EventDependency<T>::EventDependency(ComponentInterface* core, T& evt, std::set<EventHandle> deps) : EventDependencyBase(core, deps)
{
	this->evt = constructEvent<T, T&>(core, evt);
}

template<class T>
inline void EventBus::EventDependency<T>::invoke(ComponentInterface* core)
{
	core->getEventBus()->dispatchEvent<T>(core, *evt);
}
#endif