
#pragma once
#ifndef CORE_T
#define CORE_T
#include "StaticCoreEngine.h"

#include "CoreEvents.h"

#include <algorithm>



//template<typename module, typename>
//constexpr inline module* ComponentInterface::getModule()



template<typename t, typename... Ts>
ModuleContainer<t, Ts...>::ModuleContainer(ComponentInterface* c) : data(c), rest(c) {}

template<typename t, typename... Ts>
void ModuleContainer<t, Ts...>::construct(ComponentInterface* c)
{
	data.construct(c);
	rest.construct(c);
}
template<typename t, typename... Ts>
void ModuleContainer<t, Ts...>::destroy()
{
	data.destroy();
	rest.destroy();
}
template<typename t, typename... Ts>
void* ModuleContainer<t, Ts...>::get(size_t typeHash)
{
	if (typeid(t).hash_code() == typeHash)
		return &data;
	else
		return rest.get(typeHash);
}



template<typename t>
ModuleContainer<t>::ModuleContainer(ComponentInterface* c) : data(c) {}

template<typename t>
void ModuleContainer<t>::construct(ComponentInterface* c)
{
	data.construct(c);
}
template<typename t>
void ModuleContainer<t>::destroy()
{
	data.destroy();
}
template<typename t>
void* ModuleContainer<t>::get(size_t typeHash)
{
	if (typeid(t).hash_code() == typeHash)
		return &data;
	else
		return nullptr;
}



template<class... modules>
CoreEngine<modules...>::CoreEngine(const bool debugState, Version version) :
	ComponentInterface(debugState, version),
	modContainer(this)
{
}

template<class... modules>
void CoreEngine<modules...>::run()
{
	eventBus.invokeEvent(LoadEvent{}, std::vector<MemoryRange>());

	modContainer.construct(this);

	eventBus.registerHandler(this, EventBus::getEventID<ShutdownEvent>()); //registers ShutdownHandler

	pool.run();

	modContainer.destroy();
}

template<class... modules>
void CoreEngine<modules...>::operator()(const EventBase* args)
{
	if (args.type == EventBus::getEventID<ShutdownEvent>())
		pool.stop();
}

template<class... modules>
void* CoreEngine<modules...>::getDataOnType(size_t i)
{
	return modContainer.get(i);
}

#endif