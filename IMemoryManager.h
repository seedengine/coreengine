#include <type_traits>
#include <vector>
#include "IMemoryInterface.h"

#ifndef I_MEM_MGR
template<class Interface, typename AllocationDataType>//, typename std::enable_if<std::is_base_of<IMemoryInterface<Interface::AllocationType>, Interface>::value, int>::type = 0>
class IMemoryManager
{
public:
	template<typename... args>
	IMemoryManager(size_t minBlockSize, args... allocationSize) : memoryInterface(allocationSize...), MIN_BLOCK_SIZE(minBlockSize)
	{

	}

	virtual void* getData(MemoryRange data)
	{
		return ((char*)memoryInterface.getAllocation(data.memIndex)) + data.offset;
	}
	
	const size_t getAllocationCount() const
	{
		return allocationPools.size();
	}

	inline Interface* getInterface()
	{
		return &memoryInterface;
	}

	virtual MemoryRange allocate(size_t size)
	{
		return memoryInterface.allocate(size);
	}

	virtual void freeRange(MemoryRange range)
	{
		memoryInterface.freeRange(range);
	}

	virtual MemoryRange copy(MemoryRange src, MemoryRange dst)
	{
		return memoryInterface.copy(src, dst);
	}
	virtual void swap(MemoryRange src, MemoryRange dst)
	{
		memoryInterface.swap(src, dst);
	}
	virtual void set(MemoryRange range, void* data)
	{
		memoryInterface.set(range, data);
	}
	virtual void set(MemoryRange range, void* data, size_t offset, size_t size)
	{
		memoryInterface.set(range, data, offset, size);
	}

protected:
	std::vector<AllocationDataType> allocationPools;
	Interface memoryInterface;
	const size_t MIN_BLOCK_SIZE;
};
#define I_MEM_MGR
#endif