class ComponentInterface;

#ifndef CORE_ENGINE
#define CORE_ENGINE
	#include "ThreadPool.h"
	#include "Log.h"
	#include "SystemMemoryInterface.h"
	#include "FreeListMemoryManager.h"
	#include "Event.h"
	#include "IEngine.h"

	template<typename t, typename... Ts>
	class ModuleContainer
	{
		template<class... modules>
		friend class CoreEngine;
		template<typename t1, typename... Tt>
		friend class ModuleContainer;

		inline ModuleContainer(ComponentInterface* c);

		inline void construct(ComponentInterface* c);
		inline void destroy();
		inline void* get(size_t typeHash);

		t data;
		ModuleContainer<Ts...> rest;
	};

	template<typename t>
	class ModuleContainer<t>
	{
		template<class... modules>
		friend class CoreEngine;
		template<typename t1, typename... t2>
		friend class ModuleContainer;

		inline ModuleContainer(ComponentInterface* c);

		inline void construct(ComponentInterface* c);
		inline void destroy();
		inline void* get(size_t typeHash);

		t data;
	};

	//Wrapper class
	template<class... modules>
	class CoreEngine : public ComponentInterface, public EventHandler
	{
	public:
		CoreEngine(const bool debugState, Version version);
		
		void run();

		virtual void operator()(const EventBase* args);

		virtual void* getDataOnType(size_t i);

	private:
		ModuleContainer<modules...> modContainer;
	};
#endif