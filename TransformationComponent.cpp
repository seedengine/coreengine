#include "TransformationComponent.h"

const glm::vec3 TransformationComponent::originOrientation = glm::vec3(0, 0, -1);

TransformationComponent::TransformationComponent(ComponentInterface * core)
	: IObjectComponent(core),
	pos(0, 0, 0),
	orientation(),
	scale(1, 1, 1)
{
}

void TransformationComponent::translate(glm::vec3 translation)
{
	pos = pos + translation;
}

void TransformationComponent::rotate(glm::quat rotation)
{
	orientation = orientation * rotation;
}

glm::vec3 TransformationComponent::getPosition()
{
	return pos;
}

glm::vec3 TransformationComponent::getOrientation()
{
	return orientation * originOrientation;
}

glm::vec3 TransformationComponent::getScale()
{
	return scale;
}
