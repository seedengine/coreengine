#include <vector>
#include <type_traits>
#include "Identifier.h"
#include "Exceptions.h"


#ifndef ORDERED_LIST
#define ORDERED_LIST

#pragma once
template<typename T, bool Comp(const T&, const T&)>
class OrderedList
{
public:
	OrderedList();

	size_t insert(const T& obj);
	size_t insert(T&& obj);

	void erase(size_t pos);
	void clear();

	size_t size() const;

	const T& find(const T& /*needs an > function*/);
	template<typename = typename std::enable_if<is_id_pointer<T>::value && !std::is_same<T, const uint32_t>::value>>
	const T& find(const uint32_t val);


	size_t getPos(const T&) const;
	size_t getPos(const T&, size_t begin, size_t end) const;

	//what?
	template<typename = typename std::enable_if<is_id_pointer<T>::value && !std::is_same<T, const uint32_t>::value>>
	size_t getPos(const uint32_t) const;
	template<typename = typename std::enable_if<is_id_pointer<T>::value && !std::is_same<T, const uint32_t>::value>>
	size_t getPos(const uint32_t, size_t begin, size_t end) const;

	T& operator[] (size_t i);
	typename std::vector<T>::iterator begin();
	typename std::vector<T>::iterator end();

	//~OrderedList();
private:
	std::vector<T> list;
	std::mutex lock;
};

#endif
