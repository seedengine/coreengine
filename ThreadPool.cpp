#include <thread>
#include <algorithm>
#include "ThreadPool.h"
#include "Exceptions.h"
#include "StaticCoreEngine.h"

ThreadPool::ThreadPool(uint8_t cores, ComponentInterface* core) :
	tickGuard(cores + 1), //Barrier should wait for main threead and all worker threads
	cores(cores),
	threads(cores),
	execution(true),
	coreIf(core),
	idleTime(0),
	activeTaskQueue(&taskQueue),
	idleTaskQueue(&otherTaskQueue)
{
	lastLowTick = std::chrono::system_clock::now(); //used to retain ticking Speed
	lastHighTick = std::chrono::system_clock::now(); //used to retain ticking Speed
	for (uint8_t i = 0; i < cores; i++)
		threads[i] = std::thread([this]() { work(false); }); //launches threads
}

ThreadPool::~ThreadPool()
{
	stop();
	for (std::thread& t : threads)
	{
		if (t.joinable())
			t.join(); //joins threads, if they are joinable
	}
}

void ThreadPool::flush()
{
	mainQueueMut.lock(); //enter the critical area of queue modification
	while (!mainTaskQueue.empty())
	{
		//Execute a task
		Task t = std::move(mainTaskQueue.top()); //pull task
		mainTaskQueue.pop();//remove pulled task from list
		auto now = std::chrono::system_clock::now(); //time function execution
		try
		{
			t(); //execute task
		}
		catch (const FrameworkException& e)
		{
			coreIf->getLog()->writeFatal(e.what());
			coreIf->getLog()->writeFatal("Exception thrown while executing Task.");
		}
	}
	mainQueueMut.unlock(); //leave critical queue modification area
	std::scoped_lock lock(activeQueueMut); //enter the critical area of queue modification
	//activeQueueMut.lock();
	//idleQueueMut.lock(); //enter the critical area of queue modification
	while (!taskQueue.empty())
	{
		//Execute a task
		Task t = std::move(taskQueue.top()); //pull task
		taskQueue.pop();//remove pulled task from list
		auto now = std::chrono::system_clock::now(); //time function execution
		try
		{
			t(); //execute task
		}
		catch (const FrameworkException& e)
		{
			coreIf->getLog()->writeFatal(e.what());
			coreIf->getLog()->writeFatal("Exception thrown while executing Task.");
		}
	}
	while (!otherTaskQueue.empty())
	{
		//Execute a task
		Task t = std::move(otherTaskQueue.top()); //pull task
		otherTaskQueue.pop();//remove pulled task from list
		auto now = std::chrono::system_clock::now(); //time function execution
		try
		{
			t(); //execute task
		}
		catch (const FrameworkException& e)
		{
			coreIf->getLog()->writeFatal(e.what());
			coreIf->getLog()->writeFatal("Exception thrown while executing Task.");
		}
	}
	//activeQueueMut.unlock(); //leave critical queue modification area
	//idleQueueMut.unlock(); //leave critical queue modification area
}

//should return Handle
void ThreadPool::listSingleTask(const Task& t, bool main) //adds a task to be schedule in the next cycle
{
	if (main)
	{
		//std::unique_lock<std::recursive_mutex> lck(mainQueueMut);
		mainQueueMut.lock();
		mainTaskQueue.push(t); //adds task to scheduling list for main thread
		mainQueueMut.unlock();
	}
	else
	{
		std::lock_guard<std::recursive_mutex> lock(activeQueueMut);
		//idleQueueMut.lock();
		idleTaskQueue->push(t); //adds task to scheduling list for anny thread
		//idleQueueMut.unlock();
	}
}
//should return Handle
void ThreadPool::listSingleTask(Task&& t, bool main) //adds a task to be schedule in the next cycle
{
	if (main)
	{
		mainQueueMut.lock();
		mainTaskQueue.push(t); //adds task to scheduling list for main thread
		mainQueueMut.unlock();
	}
	else
	{
		std::lock_guard<std::recursive_mutex> lock(activeQueueMut);
		//idleQueueMut.lock();
		idleTaskQueue->push(t); //adds task to scheduling list for anny thread
		//idleQueueMut.unlock();
	}
}
//should return Handle
void ThreadPool::listImmediateTask(const Task& t) //adds a task to be schedule in the next cycle
{
	activeQueueMut.lock();
	activeTaskQueue->push(t); //adds task to scheduling list for any thread
	activeQueueMut.unlock();
}
//should return Handle
void ThreadPool::listImmediateTask(Task&& t) //adds a task to be schedule in the current cycle
{
	activeQueueMut.lock();
	activeTaskQueue->push(t); //adds task to scheduling list for any thread
	activeQueueMut.unlock();
}
void ThreadPool::listLowFreqTask(const ScheduledTask& t) //add new scheduled Tast to list for a low frequency update (20 TPS)
{
	std::lock_guard<std::mutex> guard(scheduleTaskLists);
	lowFreqTasks.push_back(t);

}
void ThreadPool::listHighFreqTask(const ScheduledTask& t) //add new scheduled Tast to list for a high frequency update (60 TPS)
{
	std::lock_guard<std::mutex> guard(scheduleTaskLists);
	highFreqTasks.push_back(t);
}

//add tps counter, add tick warning
void ThreadPool::work(bool main)
{
	std::recursive_mutex& queueLock = main ? mainQueueMut : activeQueueMut; //store a static reference to the corresponding mutex this needs to lock
	while (execution)
	{
		std::priority_queue<Task>& opTaskQueue = main ? mainTaskQueue : *activeTaskQueue; //store a static reference to the corresponding queue this executes on
		queueLock.lock(); //enter the critical area of queue modification
		if (!opTaskQueue.empty()) //check if queue is full
		{
			//Execute a task
			Task t = std::move(opTaskQueue.top()); //pull task
			opTaskQueue.pop();//remove pulled task from list
			queueLock.unlock(); //leave critical queue modification area
			auto now = std::chrono::system_clock::now(); //time function execution
			try
			{
				t(); //execute task
			}
			catch (FrameworkException& e)
			{
				coreIf->getLog()->writeFatal(e.what());
				coreIf->getLog()->writeFatal("Exception thrown while executing Task.");
				coreIf->stop();
			}
			t.setTime(std::chrono::system_clock::now() - now); //store the time the execution took to better plan subsequent executions
		}
		else //Commence scheduling
		{
			queueLock.unlock();//leave critical queue modification area, nothing happened
			if (scheduling.try_lock()) //register scheduling as task and swap
			{
				auto lowFrame = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - lastLowTick),
					highFrame = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - lastHighTick);//are two counters rly necessary?
				if (highFrame.count() >= 16) // 1000/60 = 16.6 -> 60 TPS (rounded)
				{
					scheduleHigh();
					lastHighTick = std::chrono::system_clock::now();
				}
				if (lowFrame.count() >= 50) // 1000/20 = 50 -> 20 TPS
				{
					scheduleLow();
					lastLowTick = std::chrono::system_clock::now();
				}

				idleTime += std::max<long long>((std::chrono::milliseconds(16) - highFrame).count(), 0) / 1000.0f;
				idleTime /= 2;

				std::this_thread::sleep_for(std::chrono::milliseconds(16) - highFrame); //sleep until cycle is full (that is too much waiting needs to me reduced by adding previous tasks too)
				{

					std::scoped_lock lock(activeQueueMut); //enter the critical area of queue modification
					//activeQueueMut.lock();
					//idleQueueMut.lock();
					std::swap(idleTaskQueue, activeTaskQueue);
					//activeQueueMut.unlock();
					//idleQueueMut.unlock();
				}

				tickGuard.wait(); //run in barrier
				scheduling.unlock(); //leave critial scheduling area
			}
			else
			{
				tickGuard.wait(); //when a thread is already scheduling just wait for all others to finish
			}
		}
	}
	tickGuard.setBarrierSize(0);//Destroy Barrier
}

void ThreadPool::scheduleHigh() //schedule high frequency tasks
{
	std::lock_guard<std::mutex> guard(scheduleTaskLists); //lock scheduled task list to avoid data races
	for (ScheduledTask& scheduledTask : highFreqTasks)
	{
		scheduledTask.avgTime += scheduledTask.task->getTime();
		scheduledTask.avgTime /= 2;
		scheduledTask.task->setTime(scheduledTask.avgTime); //calculate average time of execution for scheduling
		if (scheduledTask.main)
		{
			mainQueueMut.lock();
			mainTaskQueue.push(*scheduledTask.task); //add to task list that need to be executed next cycle
			mainQueueMut.unlock();
		}
		else
		{
			std::lock_guard<std::recursive_mutex> lock(activeQueueMut);
			//idleQueueMut.lock();
			idleTaskQueue->push(*scheduledTask.task); //adds task to scheduling list for anny thread
			//idleQueueMut.unlock();
		}
	}
}
void ThreadPool::scheduleLow() //schedule low frequency tasks
{
	std::lock_guard<std::mutex> guard(scheduleTaskLists);
	for (ScheduledTask& scheduledTask : lowFreqTasks)
	{
		scheduledTask.avgTime += scheduledTask.task->getTime();
		scheduledTask.avgTime /= 2;
		scheduledTask.task->setTime(scheduledTask.avgTime); //calculate average time of execution for scheduling
		if (scheduledTask.main)
		{
			mainQueueMut.lock();
			mainTaskQueue.push(*scheduledTask.task); //add to task list that need to be executed next cycle
			mainQueueMut.unlock();
		}
		else
		{
			std::lock_guard<std::recursive_mutex> lock(activeQueueMut);
			//idleQueueMut.lock();
			idleTaskQueue->push(*scheduledTask.task); //adds task to scheduling list for anny thread
			//idleQueueMut.unlock();
		}
	}
}
