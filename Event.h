#include <atomic>
#include <mutex>
#include <set>
#include <map>

#include "MemoryAssignment.h"
#include "Util.h"
#include "OrderedList.h"
#include "Exceptions.h"


//store dependant events in EventBus

#ifndef EVENT_CLASS
	#define EVENT_CLASS

	class ComponentInterface;

	typedef uint64_t EventHandle; //Defines Handle Type
	class EventBase
	{
	public:
		EventBase()
		{
		}

		virtual const uint64_t getID() const { return getClassID(); }
		static constexpr uint64_t getClassID() { return ::hash("Event"); }
		


		template<class T>
		constexpr typename std::enable_if<std::negation<std::is_base_of<EventBase, T>>::value, bool>::type is() const { return false; }
		template<class T>
		constexpr typename std::enable_if<std::is_base_of<EventBase, T>::value, bool>::type is() const { return getID() == T::getClassID(); }

		virtual const bool isDummy() const { return false; }
		virtual ~EventBase() {}
	};

	class DummyEvent : public EventBase
	{
	public:
		DummyEvent(uint64_t id) : id(id) {}
		virtual const uint64_t getID() const { return id; }
		static constexpr uint64_t getClassID() { return ::hash("DummyEvent"); }
		
		virtual const bool isDummy() const { return true; }

	private:
		uint64_t id;
	};

	class EventHandler //Base class for all classes that subscribe to events.
	{
	public:
		inline EventHandler(ComponentInterface* core) : id(lastID.fetch_add(1)), core(core) {} //Constructs the Evnet Handler Thread safe with a unique ID

		inline EventHandler(const EventHandler& other) : id(lastID.fetch_add(1)) {}

		inline const EventHandler& operator=(const EventHandler&) = delete;

		virtual void operator()(ComponentInterface*, const std::shared_ptr<EventBase>, bool isSync) = 0; //pure virtual method for event invocation implementation

		inline const EventHandle getHandlerID() const { return id; } //Getter for UID

		//ID Comparison
		inline bool operator==(const EventHandler& other) const;
		inline bool operator!=(const EventHandler& other) const;

		virtual ~EventHandler();
	protected:
		const uint64_t id;
		ComponentInterface* core;
		static std::atomic<uint64_t> lastID; //ID Counter
	};

	

	class Event : public EventBase
	{
	public:
		Event() : EventBase()
		{
		}
	};

	//Definition of derivate Events. Custom Data is handeled there.
	#define DEFINE_COND_EVENT(cond, class_name) class class_name :public std::enable_if<cond::value, Event>::type { public:  static constexpr uint64_t getClassID() { return ::hash(#class_name); } virtual const uint64_t getID() const override { return ::hash(#class_name); } //DEPRECATED. THIS IS NOT HOW COMPILE GUARDS WORK
	#define DEFINE_EVENT(class_name)class class_name : public Event { public: static constexpr uint64_t getClassID() { return ::hash(#class_name); } virtual const uint64_t getID() const override { return ::hash(#class_name); }

	//dependant events are need handlers


	//Container Managing the Event Subscriptions.
	class EventBus
	{
		struct EventHandlerRef //Subscripton of an Event Handler to an Event
		{
			uint64_t type;
			uint64_t id;
			EventHandler* ref = nullptr;
		};

		class EventDependencyBase : public EventHandler
		{
		public:
			EventDependencyBase() = delete;
			EventDependencyBase(const EventDependencyBase&) = delete;
			EventDependencyBase(ComponentInterface* core, std::set<uint64_t> data);
			void construct(ComponentInterface*);

			virtual void operator()(ComponentInterface* core, const std::shared_ptr<EventBase>, bool isSync = false);

			EventDependencyBase& operator=(const EventDependencyBase& other);

			bool isClear() { return events.empty(); }

			virtual void invoke(ComponentInterface*) = 0;

		private:
			std::set<uint64_t> events;
			ComponentInterface* core;
		};
		template<class T>
		class EventDependency : public EventDependencyBase
		{
		public:
			EventDependency() = delete;
			EventDependency(const EventDependency<T>&) = delete;
			explicit EventDependency(ComponentInterface* core, T& evt, std::set<uint64_t> data);

			virtual void invoke(ComponentInterface*);
		private:
			std::shared_ptr<T> evt;
		};
	public:
		EventBus(ComponentInterface*);

		void registerHandler(EventHandler* handler, uint64_t typeID);
		void registerDependencyHandler(EventDependencyBase* handler, uint64_t typeID);
		void removeHandler(const EventHandle&);

		static const MemoryRange createFence(ComponentInterface* core);

		template<class T /*determines event type*/, typename... Args>
		static std::enable_if_t<std::is_base_of<EventBase, T>::value, std::shared_ptr<T>> constructEvent(ComponentInterface* core, Args...);

		//Lists an Event to be invoked the following tick. 
		template<typename T>
		void dispatchEvent(ComponentInterface*, T& evtData);
		template<class T>
		void dispatchEvent(ComponentInterface*, T&, std::set<uint64_t> dependencies);
		//Invokes Event within the same thread to force blocking execution.
		template<typename T>
		void invokeEventSynchonous(ComponentInterface*, T& evtData);

		void destroy();

		~EventBus();
	protected:
		std::atomic_bool running;
		ComponentInterface* core;

		std::recursive_mutex eventMutex;

		std::map<uint64_t, std::vector<EventHandlerRef>> eventHandler;
		std::set<EventHandle> removeBuffer;

		std::set<uint64_t> invokedDependencies;

		static constexpr bool Comp(const EventHandlerRef& ref, const EventHandlerRef& handle) { return ref.type >= handle.type; }
		OrderedList<EventHandlerRef, Comp> dependencyList;
		std::atomic<EventHandle> dependencyIDCounter;
		std::map<EventHandle, EventDependencyBase*> dependencyContainer;

		void postEventInvokation(ComponentInterface* core, const std::shared_ptr<EventBase>);
		void invokeEvent(ComponentInterface*, const std::shared_ptr<EventBase>);
	};

#endif