#include <set>
#include "IEngine.h"
#include "Event.h"
#include "Transformation.h"

#pragma once

#ifndef OBJECT_COMPONENT_INTERFACE

//Part of something that in some way changes the simulated Scene
class IEngineObject;
class IObjectComponent
{
	friend IEngineObject;
public:
	IObjectComponent(ComponentInterface* parent)
		: coreHandler(parent)
	{}

	virtual void operator()(ComponentInterface*, const std::shared_ptr<EventBase> handle, bool isSync) = 0;

	virtual void onReparent() {}
	virtual void onPositionUpdate(bool intern) {}

	~IObjectComponent()
	{}
	virtual inline void listRequirements(std::set<uint64_t>& req) { throw FrameworkException("Bad call to virtual ObjectComponent::listRequirements function.");}
protected:
	ComponentInterface* coreHandler;
	IEngineObject* parent;
};
class IEngineObject
{
public:
	std::set<uint64_t>& getEventIDs() { return callbacks; }
	~IEngineObject() {}

	void add(IEngineObject* other)
	{
		children.push_back(other);
		std::set<uint64_t>& tmp = other->getEventIDs();
		callbacks.insert(tmp.begin(), tmp.end());
		if (parent != nullptr)
			parent->callbacks.insert(callbacks.begin(), callbacks.end());
		other->parent = this;
		if (root == nullptr)
		{
			other->root = this;
		}
		else
		{
			other->root = root;
		}
	}

	virtual void operator()(ComponentInterface* core, const std::shared_ptr<EventBase> evt, bool isSync)
	{
		for (IObjectComponent* component : components)
			component->operator()( core, evt, isSync);
		for (IEngineObject* child : children)
			if (child->callbacks.find(evt->getID()) != child->callbacks.end())
				if (isSync)
					child->operator()(core, evt, true);
				else
					core->getThreadPool()->listImmediateTask(
						Task([child, core, evt]() {
							child->operator()(core, evt, false); 
					}));
	}

	inline glm::mat4 getTransform() { return t; }
	inline glm::mat4 getInverseTransform() { return i_t; }
	inline Transformation& getObjTransform() { return trans; }
	inline void __thiscall updatePos(bool intern = true)
	{
		updateMatrix(parent->getTransform(), intern);
	}
	
protected:
	void __thiscall add(IObjectComponent* other)
	{
		components.push_back(other);
		other->listRequirements(callbacks);

		other->parent = this;

		other->onPositionUpdate(true);
	}

	void __thiscall updateMatrix(glm::mat4 trans, bool intern = true)
	{
		t = trans * this->trans.getMatrix();
		i_t = glm::inverse(t);
		for (IObjectComponent* component : components)
			component->onPositionUpdate(intern);
	}

	std::set<uint64_t> callbacks;
private:
	IEngineObject* root;
	IEngineObject* parent;
	std::vector<IEngineObject*> children;
	std::vector<IObjectComponent*> components;

	Transformation trans;
	glm::mat4 t;
	glm::mat4 i_t;
};


#define OBJECT_COMPONENT_INTERFACE
#endif