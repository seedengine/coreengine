
#ifndef CORE_EXCEPT
	#define CORE_EXCEPT
	#include <stdexcept>
	#include <ctime>

	//needs Error Levels
	class FrameworkException : public std::runtime_error //writes current Time in crash Log. Base Class for all Engine Errors
	{
	public:
		FrameworkException(std::string msg) : 
			std::runtime_error(convertMsg(msg).c_str())
		{
		}
		FrameworkException(const char* msg) :
			std::runtime_error(convertMsg(msg).c_str())
		{
		}
	private:
		std::string convertMsg(std::string& msg)
		{
			time_t rawtime{};
			struct tm timeinfo;
			char buffer[128];

			time(&rawtime);
			#ifdef _WIN32
				localtime_s(&timeinfo, &rawtime);
			#else
				timeinfo = *localtime(&rawtime);
			#endif

			strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", &timeinfo);
			std::string str(buffer);
			return "[" + str + "] " + msg;
		}
		std::string convertMsg(const char* msg)
		{
			time_t rawtime{};
			struct tm timeinfo;
			char buffer[128];

			time(&rawtime);
			#ifdef _WIN32
				localtime_s(&timeinfo, &rawtime);
			#else
				timeinfo = *localtime(&rawtime);
			#endif

			strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", &timeinfo);
			std::string str(buffer);
			return "[" + str + "] " + msg;
		}
	};
#endif
