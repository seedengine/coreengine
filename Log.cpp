#include <ctime>
#include <utility>

#include "Log.h"



Log::Log(bool debug, ThreadPool* threadPool) : logFile("Engine.log"), debug(debug), loggingTask([this]() {pullMessages(); })
{
	if (debug)
		writeInfo("Debugging enabled.");
	
	threadPool->listLowFreqTask(ScheduledTask(&loggingTask, false));
	writeInfo("Registered Log Thread.");

	time_t rawtime{};
	struct tm timeinfo;
	char buffer[40];

	time(&rawtime);
	#ifdef _WIN32
		localtime_s(&timeinfo, &rawtime);
	#else
		timeinfo = *localtime(&rawtime);
	#endif

	strftime(buffer, sizeof(buffer), "%d-%m-%Y", &timeinfo);
	output(std::string("Logging started at ") + buffer);
}

void Log::writeInfo(std::string& msg)
{
	std::unique_lock<std::mutex> guard(infoMut);
	info.push( "[INFO] " + msg);
}

void Log::writeWarning(std::string& msg)
{
	std::unique_lock<std::mutex> guard(warningMut);
	warning.push("[WARNING] " + msg);
}


void Log::writeError(std::string& msg)
{
	std::unique_lock<std::mutex> guard(errorMut);
	error.push("[ERROR] " + msg);
}

void Log::writeFatal(std::string& msg)
{
	std::unique_lock<std::mutex> guard(fatalMut);
	fatal.push("[FATAL] " + msg);
}

void Log::writeInfo(const char * msg)
{
	std::unique_lock<std::mutex> guard(infoMut);
	info.push("[INFO] " + std::string(msg));
}

void Log::writeWarning(const char * msg)
{
	std::unique_lock<std::mutex> guard(warningMut);
	warning.push("[WARNING] " + std::string(msg));
}

void Log::writeError(const char * msg)
{
	std::unique_lock<std::mutex> guard(errorMut);
	error.push("[ERROR] " + std::string(msg));
}

void Log::writeFatal(const char * msg)
{
	std::unique_lock<std::mutex> guard(fatalMut);
	fatal.push("[FATAL] " + std::string(msg));
}

void Log::flush()
{
	while (hasLogQueued())
		pullMessages();
}

Log::~Log()
{
	//flush();
	output(std::string("[INFO] Shutting down.")); //reguister rescheduling shutdown task
	logFile.close();
}

void Log::output(const std::string& str)
{
	logFile.write(str.c_str(), str.size())
		.write("\n", 1);
	printf(str.c_str());
	printf("\n");
	logFile.flush();
}

void Log::pullMessages()
{
	while (!fatal.empty())
	{
		std::unique_lock<std::mutex> lock(fatalMut);
		if (!fatal.empty())
		{
			output(fatal.front());
			fatal.pop();
		}
	}
	while (!error.empty())
	{
		std::unique_lock<std::mutex> lock(errorMut);
		if (!error.empty())
		{
			output(error.front());
			error.pop();
		}
	}
	while (!warning.empty())
	{
		std::unique_lock<std::mutex> lock(warningMut);
		if (!warning.empty())
		{
			output(warning.front());
			warning.pop();
		}
	}
	while (!info.empty())
	{
		std::unique_lock<std::mutex> lock(infoMut);
		if (!info.empty())
		{
			output(info.front());
			info.pop();
		}
	}
}
