#pragma once
#include "StaticCoreEngine.h"
#include "IObjectComponent.h"

class Scene : public EventHandler
{
public:
	Scene(ComponentInterface* core) : EventHandler(core)
	{

	}
	~Scene()
	{

	}

	void add(IEngineObject* other)
	{
		root.add(other);
	}

	void registerEvents(ComponentInterface* core)
	{
		for (const uint64_t& id : root.getEventIDs())
		{
			core->getEventBus()->registerHandler(this, id);
		}
	}

	void operator()(ComponentInterface* core, const std::shared_ptr<EventBase> evt, bool isSync)
	{
		root(core, evt, isSync);
	}

private:
	IEngineObject root;
};
