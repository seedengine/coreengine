#include <cstdint>

#pragma once
#ifndef FUNCTIONAL_INTERFACE
class IFunctionalComponent
{
public:
	IFunctionalComponent() {}

	virtual void __cdecl doAction(uint32_t, void*) = 0;

	~IFunctionalComponent() {}
};
#define FUNCTIONAL_INTERFACE
#endif