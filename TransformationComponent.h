#include "IObjectComponent.h"

#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>

#pragma once
class TransformationComponent : public IObjectComponent
{
public:
	TransformationComponent(ComponentInterface*);

	void translate(glm::vec3);
	void rotate(glm::quat rotation);

	glm::vec3 getPosition();
	glm::vec3 getOrientation();
	glm::vec3 getScale();

private:
	glm::vec3 pos;
	const static glm::vec3 originOrientation;
	glm::quat orientation;
	glm::vec3 scale;
};

