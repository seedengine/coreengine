#include "Exceptions.h"

//Base Interface for a Wrapper class
#pragma once
#ifndef IHANDLE_CLASS
template<typename handle>
class IHandle
{
public:
	const handle& getHandle() const {
		if (!isInit)
			throw FrameworkException("Trying to acquire handle that has not been initialized."); 
		return _getHandle();
	}
	inline operator handle() const { return this->getHandle(); }
	inline bool isInitialized() { return isInit; }
protected:
	virtual const handle& _getHandle() const = 0;
	void setInitialized() { isInit = true; }
private:
	bool isInit = false;
};
#define IHANDLE_CLASS
#endif