#include <atomic>
#include <functional>
#include <vector>

#include "Exceptions.h"

#pragma once
//A Generic class describing a rotating collection elements of dynamic counts.
template<class T>
class ISwappable
{
public:
	ISwappable() : items(), pos(0) {} //creates an empty swap collection
	ISwappable(const ISwappable& other) : items(other.items) //copy constructor
	{
		pos.store(other.pos.load());
	}
	ISwappable(const size_t size) :items(size), pos(0) //constructs a collection of size length. Using the default constructor
	{}

	void resize(const size_t size) //change dynamic size. and force reset the counter to deal with shrinking.
	{
		items.resize(size);
		pos.store(0);
	}


	void execute(std::function<void(T&, size_t)>f) //execute a function on each element of the collection
	{
		if (items.empty())
		{
			throw FrameworkException("Trying to iterate over empty SwapList.");
			return;
		}
		uint32_t i = 0;
		for (T& item : items)
		{
			f(item, i);
			i += 1;
		}
	}
	template<typename... Args>
	void executeIndexed(std::function<void(T&, size_t, Args...)> f, Args... args) //execute a function with parameters on each element of the collection
	{
		if (items.empty())
		{
			throw FrameworkException("Trying to iterate over empty SwapList.");
			return;
		}
		uint32_t i = 0;
		for (T& item : items)
		{
			f(item, i, args...);
			i += 1;
		}
	}
	template<typename... Args>
	void execute(std::function<void(T&, Args...)> f, Args... args) //execute a function with parameters on each element of the collection
	{
		if (items.empty())
		{
			throw FrameworkException("Trying to iterate over empty SwapList.");
			return;
		}
		for (T& item : items)
		{
			f(item, args...);
		}
	}

	void init() //construct objects -- needs to be limited to classes having this function
	{
		uint32_t i = 0;
		for (T& item : items)
		{
			item.construct(i);
			i += 1;
		}
	}
	//construct objects -- needs to be limited to classes having this function
	template<typename... Args>
	void init(Args... args)
	{
		uint32_t i = 0;
		for (T& item : items)
		{
			item.construct(args..., i);
			i += 1;
		}
	}

	void prev() //select previous item in the collection
	{
		if (!pos.compare_exchange_weak((size_t&)0, getLastIdx()))
			pos--;
	}
	void next() //select next item in the collection
	{
		size_t val = getLastIdx();
		if (!pos.compare_exchange_weak(val, (size_t)0))
			pos++;
	}

	const T& getCurrent() const //gets reference to the current selected item
	{
		return items[pos];
	}
	T& getCurrentRef() //gets reference to the current selected item
	{
		return items[pos];
	}
	T& getPrev() //gets reference to the previous selected item
	{
		return items[ (getLastIdx + pos - 1) % items.size()];
	}
	T& getNext() //gets reference to the next selected item
	{
		return items[(pos + 1) % items.size()];
	}

	const size_t getIdx() const //get current position in collection
	{
		return pos.load();
	}
	constexpr inline std::vector<T> getList() const
	{
		return items;
	}
	constexpr inline const size_t getSize() const
	{
		return items.size();
	}
	constexpr inline const size_t getLastIdx() const
	{
		return items.size() - 1;
	}
	constexpr inline const std::atomic<size_t>& getCurrIdx() const
	{
		return pos;
	}
	inline void setCurrPos(uint32_t newPos)
	{
		if (newPos <= getLastIdx())
			pos.store(newPos);
	}

private:
	std::atomic<size_t> pos;
	std::vector<T> items;
};