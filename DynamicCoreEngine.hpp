#include "DynamicCoreEngine.h"
#include "Exceptions.h"

#pragma once

template<typename ReturnType, typename... Args>
std::enable_if_t<std::is_void<ReturnType>::value, void> DynamicCoreEngine::invoke(std::string libName, std::string function, Args... args)
{
	try
	{
		if (loadedLibraries.find(libName) == loadedLibraries.end())
		{
			std::pair<const std::string, Library> p(libName, Library());
			p.second = loadLibrary(libName);
			loadedLibraries.insert(p);
		}
		return invokeFunction<ReturnType, Args...>(loadedLibraries[libName].handle, function, args...);
	}
	catch (FrameworkException& exception)
	{
		this->getLog()->writeFatal(exception.what());
	}
	return;
}
template<typename ReturnType, typename... Args>
std::enable_if_t<std::is_pointer<ReturnType>::value, ReturnType> DynamicCoreEngine::invoke(std::string libName, std::string function, Args... args)
{
	try
	{
		if (loadedLibraries.find(libName) == loadedLibraries.end())
		{
			std::pair<const std::string, Library> p(libName, Library());
			p.second = loadLibrary(libName);
			loadedLibraries.insert(p);
		}
		return invokeFunction<ReturnType, Args...>(loadedLibraries[libName].handle, function, args...);
	}
	catch (FrameworkException& exception)
	{
		this->getLog()->writeFatal(exception.what());
	}
	return nullptr;
}

template<typename ReturnType, typename... Args>
std::enable_if_t<std::conjunction<std::is_trivially_constructible<ReturnType>, std::negation<std::is_void<ReturnType>>, std::negation<std::is_pointer<ReturnType>>>::value, ReturnType> DynamicCoreEngine::invoke(std::string libName, std::string function, Args... args)
{
	try
	{
		if (loadedLibraries.find(libName) == loadedLibraries.end())
		{
			std::pair<const std::string, Library> p(libName, Library());
			p.second = loadLibrary(libName);
			loadedLibraries.insert(p);
		}
		return invokeFunction<ReturnType, Args...>(loadedLibraries[libName].handle, function, args...);
	}
	catch (FrameworkException& exception)
	{
		this->getLog()->writeFatal(exception.what());
	}
	
	return ReturnType();
}



#ifdef WIN
template<typename ReturnType, typename... Args>
ReturnType DynamicCoreEngine::invokeFunction(DynamicCoreEngine::DynLibHandle& handle, std::string name, Args... args) const
{
	typedef ReturnType(__cdecl *FuncType)(Args...);
	FuncType func;
	func = (FuncType)GetProcAddress(handle, name.c_str());
	if (func != nullptr)
		return func(args...);
	else
		throw FrameworkException("Failed to call " + name + " failed. Error Code: " + std::to_string(GetLastError()));
}
#else
template<typename ReturnType, typename... Args>
ReturnType DynamicCoreEngine::invokeFunction(const DynamicCoreEngine::DynLibHandle& handle, std::string name, Args...) const
{
	typedef ReturnType(__cdecl *FuncType)(Args...);
	FuncType func;
	func = (FuncType)dlsym(handle, name.c_str());
	if (func != nullptr)
		return func(args...);
	else
		throw FrameworkException("Failed to call " + name + " failed. Function could not be resolved.");
}
#endif