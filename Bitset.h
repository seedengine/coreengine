#include <cstdint>
#include <vector>
#pragma once

#if _WIN32 || _WIN64
#if _WIN64
#define ENVIRONMENT64
#else
#define ENVIRONMENT32
#endif
#endif

// Check GCC
#if __GNUC__
#if __x86_64__ || __ppc64__
#define ENVIRONMENT64
#else
#define ENVIRONMENT32
#endif
#endif

class Bitset
{
#ifdef ENVIRONMENT64
	typedef uint64_t blockType;
	const size_t blockSize = 64;
#elif defined(ENVIRONMENT32)
	typedef uint32_t blockType;
	const size_t blockSize = 32;
#endif
public:

	//create compile time fuinction that generates the mask
	Bitset() : currSize(0)
	{

	}


	//void set(size_t* data, size_t offset);
	void push(bool data);

	size_t size();
	const bool at(size_t offset);
	const bool operator[] (size_t i);

private:
	std::vector<blockType> dataArr;
	size_t currSize;
};