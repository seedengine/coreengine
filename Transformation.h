#define _USE_MATH_DEFINES

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#pragma once
#ifndef TRANSFORMATION_WRAPPER
class Transformation
{
public:
	inline Transformation() :
	_translation(0.0f, 0.0f, 0.0f), 
		_rotation(0.0f, 0.0f, 0.0f), 
		_scale(1.0f, 1.0f, 1.0f)
	{}
	inline Transformation(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale) :
		_translation(translation), _rotation(rotation), _scale(scale)
	{}

	inline void setTranslation(glm::vec3 trans)
	{
		_translation = trans;
	}
	inline glm::vec3 getTranslation()
	{
		return _translation;
	}
	inline void translate(glm::vec3 trans)
	{
		_translation += trans;
	}

	inline void setRotation(glm::vec3 rot)
	{
		_rotation = rot;
	}
	inline glm::vec3 getRotation()
	{
		return _rotation;
	}
	inline void rotate(glm::vec3 rot)
	{
		_rotation += rot;
	}

	inline void setScale(glm::vec3 scale)
	{
		_scale = scale;
	}
	inline glm::vec3 getScale()
	{
		return _scale;
	}
	inline void scale(glm::vec3 scale)
	{
		_scale += scale;
	}
	
	inline Transformation copy()
	{
		return Transformation(_translation, _rotation, _scale);
	}

	~Transformation() {}

	inline glm::mat4 getTransMat()
	{
		return glm::translate(glm::mat4(), _translation);
	}

	glm::mat4 getRotMat()
	{
		glm::mat4 rx = glm::mat4();
		float x = (float)(_rotation.x * M_PI / 180.0f);
		rx[1][1] = cosf(x);			rx[1][2] = -sinf(x);
		rx[2][1] = sinf(x);			rx[2][2] = cosf(x);

		glm::mat4 ry = glm::mat4();
		float y = (float)(_rotation.y * M_PI / 180.0f);
		ry[0][0] = cosf(y);			ry[0][2] = -sinf(y);
		ry[2][0] = sinf(y);			ry[2][2] = cosf(y);

		glm::mat4 rz = glm::mat4();
		float z = (float)(_rotation.z * M_PI / 180.0f);
		rz[0][0] = cosf(z);			rz[0][1] = -sinf(z);
		rz[1][0] = sinf(z);			rz[1][1] = cosf(z);

		return rz*ry*rx;
	}
	inline glm::mat4 getScaleMat()
	{
		return glm::scale(glm::mat4(), _scale);
	}
	inline glm::mat4 getMatrix()
	{
		return getTransMat() * getRotMat() * getScaleMat();
	}

	glm::vec3 _translation;
	glm::vec3 _rotation;
	glm::vec3 _scale;
};
#define TRANSFORMATION_WRAPPER
#endif