#include <queue>
#include <string>
#include <iostream>
#include <future>

#include "IEngine.h"
#include "ThreadPool.h"
#include "CoreEvents.h"

#pragma once

#define WIN

#ifdef WIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <conio.h>
#else
#include <unistd.h>
#endif

class CommandLineInterface : public IEngineModule
{
public:
	CommandLineInterface(ComponentInterface* core) :
		IEngineModule(core),
		running(true),
		blockedThread([this] { pushInput(); }),
		task([this] { pullInput(); })
	{
		core->getThreadPool()->listLowFreqTask(ScheduledTask{ &task, false });
	}

	virtual void construct(ComponentInterface*) {}
	virtual void destroy()
	{
		std::unique_lock<std::mutex> guard(stateMutex);
		if (!running)
			return;
		running = false;
		simulateInput();
		if (blockedThread.joinable())
			blockedThread.join();
	}

	void __cdecl doAction(uint32_t, void*)
	{}

	void pullInput()
	{
		std::unique_lock<std::mutex> guard(commandMut);
		while (!commands.empty())
		{
			CommandInputEvent evt;
			evt.setCommand(commands.front());
			core->getEventBus()->dispatchEvent(core, evt);
			commands.pop();
		}
	}

	void pushInput()
	{
		std::string s;
		while (running)
		{
			std::getline(std::cin, s);
			{
				std::unique_lock<std::mutex> guard(commandMut);
				commands.push(s);
			}
		}
	}

	~CommandLineInterface()
	{
		destroy();
	}

	virtual const std::string getID() const { return "CommandLineInterface"; }
private:
#ifdef WIN
	void simulateInput()
	{
		INPUT_RECORD virtNewLine[2];
		DWORD res = 0;
		//Press Event
		virtNewLine[0].EventType = KEY_EVENT;
		virtNewLine[0].Event.KeyEvent.bKeyDown = TRUE;
		virtNewLine[0].Event.KeyEvent.wRepeatCount = 1;
		virtNewLine[0].Event.KeyEvent.wVirtualKeyCode = VK_SPACE;
		virtNewLine[0].Event.KeyEvent.wVirtualScanCode = MapVirtualKey(VK_SPACE, MAPVK_VK_TO_VSC);
		virtNewLine[0].Event.KeyEvent.uChar.UnicodeChar = VK_SPACE;
		virtNewLine[0].Event.KeyEvent.dwControlKeyState = 0x0000;

		//Release Event (So that the key doesn't lock)
		virtNewLine[1].EventType = KEY_EVENT;
		virtNewLine[1].Event.KeyEvent.bKeyDown = TRUE;
		virtNewLine[1].Event.KeyEvent.wRepeatCount = 1;
		virtNewLine[1].Event.KeyEvent.wVirtualKeyCode = VK_RETURN;
		virtNewLine[1].Event.KeyEvent.wVirtualScanCode = MapVirtualKey(VK_RETURN, MAPVK_VK_TO_VSC);
		virtNewLine[1].Event.KeyEvent.uChar.UnicodeChar = VK_RETURN;
		virtNewLine[1].Event.KeyEvent.dwControlKeyState = 0x0000;
		if (WriteConsoleInput(GetStdHandle(STD_INPUT_HANDLE), virtNewLine, 2, &res) == 0)
		{
			std::string tmp = std::to_string(GetLastError());
			core->getLog()->writeFatal(tmp);
		}

		if (res != 2)
			throw FrameworkException("[WIN] Failed to send wirtual return keypress to the console.");
	}
#else
	void simulateInput() //untested
	{
		std::string s = "\n";
		write(STDIN_FILENO, s.c_str(), s.size());
	}
#endif
	Task task;
	bool running;
	std::mutex commandMut;
	std::mutex stateMutex;
	std::queue<std::string> commands;
	std::thread blockedThread;
};
