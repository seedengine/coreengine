#include "IEngine.h"

ComponentInterface::~ComponentInterface()
{
	eventBus.destroy();
	pool.stop();
	logger.flush();
}

ComponentInterface::ComponentInterface(bool debugState) :
	debugState(debugState),
	pool(std::thread::hardware_concurrency(), this),
	logger(debugState, &pool),
	memoryHandler(32),
	eventBus(this),
	running(true)
{
}
