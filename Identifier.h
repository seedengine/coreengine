#include <atomic>

#pragma once
#ifndef IDENTIFIER
class Identifier;

template <typename class_pointer>
struct is_id_pointer
	: std::integral_constant<bool, std::is_pointer<class_pointer>::value
	&& std::is_base_of<Identifier,
	typename std::remove_pointer<class_pointer>::type
	>::value>
{
};

class Identifier
{
public:
	Identifier() : id(cntr++) {}

	constexpr const uint32_t getID() const { return id; }
	constexpr operator uint32_t() const { return id; }

	//template<class T, typename = typename std::enable_if<is_id_pointer<T>::value>, typename = typename std::enable_if<std::logical_not<std::is_pointer<T>>::value>>
	//constexpr static bool cmp(const T& a, const T& b) { return a.getID() >= b.getID(); }
	template<class T, typename = typename std::enable_if<is_id_pointer<T>::value>, typename = typename std::enable_if<std::is_pointer<T>::value>>
	constexpr static bool cmp(const T& a, const T& b) { return a->getID() >= b->getID(); }
protected:
	Identifier(uint32_t id) : id(id) {}
private:
	static std::atomic<uint32_t> cntr;
	const uint32_t id;
};



#define IDENTIFIER
#endif