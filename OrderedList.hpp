#include "OrderedList.h"

#ifndef ORDERED_LIST_T
#define ORDERED_LIST_T

#include <cmath>

template<typename T, bool Comp(const T&, const T&)>
OrderedList<T, Comp>::OrderedList() {}

template<typename T, bool Comp(const T&, const T&)>
size_t OrderedList<T, Comp>::insert(const T& obj)
{
	std::lock_guard<std::mutex> lck(lock);
	return list.insert(list.begin() + getPos(obj), obj) - list.begin();
}
template<typename T, bool Comp(const T&, const T&)>
size_t OrderedList<T, Comp>::insert(T&& obj)
{
	std::lock_guard<std::mutex> lck(lock);
	return list.insert(list.begin() + getPos(obj), obj) - list.begin();
}

template<typename T, bool Comp(const T&, const T&)>
void OrderedList<T, Comp>::erase(size_t pos)
{
	std::lock_guard<std::mutex> lck(lock);
	list.erase(list.begin() + pos);
}

template<typename T, bool Comp(const T&, const T&)>
inline void OrderedList<T, Comp>::clear()
{
	std::lock_guard<std::mutex> lck(lock);
	list.clear();
}

template<typename T, bool Comp(const T&, const T&)>
size_t OrderedList<T, Comp>::size() const
{
	return list.size();
}

template<typename T, bool Comp(const T&, const T&)>
const T& OrderedList<T, Comp>::find(const T& val)
{
	std::lock_guard<std::mutex> lck(lock);
	if (list.size() == 1)
		return list[0];
	else
	{
		size_t pos = getPos(val);
		if (pos == -1 || pos >= list.size())
			throw FrameworkException("Element not found in List");
		return list[pos];
	}
}

template<typename T, bool Comp(const T&, const T&)>
template<typename U>
const T& OrderedList<T, Comp>::find(const uint32_t val)
{
	std::lock_guard<std::mutex> lck(lock);
	if (list.empty())
		throw FrameworkException("Trying to access Element of empty list.");
	else if (list.size() == 1)
		return list[0];
	else
	{
		size_t pos = getPos(val);
		if (pos == -1 || pos >= list.size())
			throw FrameworkException("Element not found in List");
		return list[pos];
	}
}

template<typename T, bool Comp(const T&, const T&)>
inline size_t OrderedList<T, Comp>::getPos(const T& val) const
{
	return getPos(val, 0, list.size());
}

template<typename T, bool Comp(const T&, const T&)>
inline size_t OrderedList<T, Comp>::getPos(const T& val, size_t begin, size_t end) const
{
	size_t range = end - begin;

	if (list.empty())
		return 0;
	else if (range == 0)
		return begin;
	else
		if (Comp(list[begin + (range >> 1)], val))
			return getPos(val, begin, begin + (range >> 1));
		else
			return getPos(val, begin + (range >> 1) + (range & 1 ? 1 : 0), end);
}

template<typename T, bool Comp(const T&, const T&)>
T& OrderedList<T, Comp>::operator[] (size_t i)
{
	return list[i];
}

template<typename T, bool Comp(const T&, const T&)>
typename std::vector<T>::iterator OrderedList<T, Comp>::begin()
{
	return list.begin();
}
template<typename T, bool Comp(const T&, const T&)>
typename std::vector<T>::iterator OrderedList<T, Comp>::end()
{
	return list.end();
}
template<typename T, bool Comp(const T&, const T&)>
template<typename U>
inline size_t OrderedList<T, Comp>::getPos(const uint32_t val) const
{
	return getPos(val, 0, list.size());
}

template<typename T, bool Comp(const T&, const T&)>
template<typename U>
size_t OrderedList<T, Comp>::getPos(const uint32_t val, size_t begin, size_t end) const
{
	size_t range = end - begin;

	if (list.empty())
		return 0;
	else if (range == 0)
		return begin;
	else
		if (list[begin + (range >> 1)]->getID() >= val)
			return getPos(val, begin, begin + (range >> 1));
		else
			return getPos(val, begin + (range >> 1) + (range & 1 ? 1 : 0), end);
}
#endif
