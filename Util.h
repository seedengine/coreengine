#pragma once
#include "IHandle.h"
#include <atomic>
#include <mutex>
#include <type_traits>

#include <climits>

#ifndef UTIL

/*
template<typename T>
class LockedRef : IHandle<T>
{
public:
	LockedRef(T* ref, std::mutex* mut) : handle(ref), mut(mut)
	{
		mut->lock();
	}

	virtual const T& _getHandle() const
	{
		return *handle;
	}

	virtual T& getReference() const
	{
		return *handle;
	}

	~LockedRef()
	{
		mut->unlock();
	}
private:
	T* handle;
	std::mutex* mut;
};
*/
//fast log2 algorithm
constexpr uint64_t fastLog2(uint64_t value)
{
	const int tab64[64] = {
		63,  0, 58,  1, 59, 47, 53,  2,
		60, 39, 48, 27, 54, 33, 42,  3,
		61, 51, 37, 40, 49, 18, 28, 20,
		55, 30, 34, 11, 43, 14, 22,  4,
		62, 57, 46, 52, 38, 26, 32, 41,
		50, 36, 17, 19, 29, 10, 13, 21,
		56, 45, 25, 31, 35, 16,  9, 12,
		44, 24, 15,  8, 23,  7,  6,  5 };
	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	value |= value >> 32;
	return tab64[((uint64_t)((value - (value >> 1)) * 0x07EDD5E59A4E28C2)) >> 58];
}

// djb2 hashing algorithm
#ifdef WIN32
#pragma warning(disable:4307)
#endif // WIN32
constexpr const uint32_t hash(const char *str)
{
	uint32_t hash = 5381;
	int c = 0;

	while ((c = *str++))
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

//greatest common denominator, recursive (euklidian algorithm)
inline constexpr uint64_t gcd(uint64_t a, uint64_t b)
{
	if(b)
		return gcd(b, a % b);
	else
		return a;
}

//https://stackoverflow.com/questions/105252/how-do-i-convert-between-big-endian-and-little-endian-values-in-c
template <typename T>
T swap_endian(T u)
{
	static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

	union
	{
		T u;
		unsigned char u8[sizeof(T)];
	} source, dest;

	source.u = u;

	for (size_t k = 0; k < sizeof(T); k++)
		dest.u8[k] = source.u8[sizeof(T) - k - 1];

	return dest.u;
}
#define UTIL
#endif