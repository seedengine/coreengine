#pragma once
#include <type_traits>
#include <assert.h>

#include "DataStructures.h"
#include "IMemoryInterface.h"
#include "Util.h"


struct BlockField
{
	//limit ptrSize
	BlockField* prev;
	BlockField* next;
	uint8_t data; //order, tag
};

//Free List Based Manager
template<class Interface>
class BuddyMemMgr
{
	
public:
	MemoryRange getMemoryRange(size_t size)
	{
		size = getNextPower(size);
		uint8_t sizeIndex = BlockAssignment::getSizeIndex(MIN_BLOCK_SIZE, size);
		BlockAssignment* block = getNextFreeBlock(sizeIndex);
		size_t pos = block->getFreeOffset(sizeIndex) * (MIN_BLOCK_SIZE << sizeIndex);

		return MemoryRange(block->getPoolIndex(), pos, size);
	}
	void clearMemoryRange(MemoryRange range)
	{
		pools[range.memIndex].free(BlockAssignment::getSizeIndex(MIN_BLOCK_SIZE, range.size), range.offset);
	}

	inline MemoryRange copy(MemoryRange src, MemoryRange dst)
	{
		return _interface.copy(src, dst);
	}
	inline void swap(MemoryRange src, MemoryRange dst)
	{
		_interface.swap(src, dst);
	}
	inline void set(MemoryRange range, void* data)
	{
		_interface.set(range, data);
	}

	inline void* getData(MemoryRange data)
	{
		return ((char*)_interface.getAllocation(data.memIndex)) + data.offset;
	}

	inline Interface* getInterface()
	{
		return &_interface;
	}

private:
	Interface _interface;

	const uint32_t MIN_BLOCK_SIZE;
	std::vector<void*> pools;
};
