#ifndef MEM_IF
#define MEM_IF
	#include <vector>
	#include "MemoryAssignment.h"

	//Base Interface for handling Memory in accordance with Pools
	template<typename AllocationDataType = void*>
	class IMemoryInterface
	{
	public:
		IMemoryInterface(size_t size) : allocationSize(size) {}

		typedef AllocationDataType AllocationType;
		virtual MemoryRange allocate(size_t) = 0;
		virtual void freeRange(MemoryRange memory) = 0;
		virtual void set(MemoryRange, void*) = 0;
		virtual void set(MemoryRange, void*, size_t, size_t) = 0;

		virtual size_t getSize() { return allocationSize; }

		const size_t getAllocationCount() const
		{
			return allocations.size();
		}

		virtual MemoryRange copy(MemoryRange src, MemoryRange dst) = 0;
		virtual void swap(MemoryRange src, MemoryRange dst) = 0;


		template<typename ReturnType = AllocationDataType>
		inline typename std::enable_if_t<std::is_pointer<ReturnType>::value && std::is_same<ReturnType, AllocationDataType>::value, const ReturnType> getAllocation(size_t index) const
		{
			return allocations[index];
		}
		template<typename ReturnType = AllocationDataType>
		inline typename std::enable_if_t<std::is_same<ReturnType, AllocationDataType>::value && !std::is_pointer<ReturnType>::value, const ReturnType*> getAllocation(size_t index) const
		{
			return &allocations[index];
		}

	protected:
		std::vector<AllocationDataType> allocations;
		const size_t allocationSize;
	};
#endif