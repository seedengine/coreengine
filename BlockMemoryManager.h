#pragma once
#include "IMemoryManager.h"
#include "MemoryAssignment.h"
#include "Identifier.h"


/*
allocation scheme: arbitrary size allocations taht are tracked by a UID.
*/


template<class T>
class BlockMemoryManager :
	public IMemoryManager<T, size_t>
{
public:

	BlockMemoryManager(uint32_t minBlockSize) : IMemoryManager<T, size_t>(minBlockSize, minBlockSize * 32/*amount of alocations per set*/ * 252/*sum of 2^n where n are the different allocations*/)
	{
	}
	~BlockMemoryManager()
	{

	}

	virtual MemoryRange allocate(size_t size)
	{
		//MemoryRange r{Identifier().getID(), 0, size};
		return this->memoryInterface.allocate(size);
	}

	void clearMemoryRange(MemoryRange range)
	{
		if (range.memIndex == -1)
			return;
		this->memoryInterface.freeRange(range);
	}
};

